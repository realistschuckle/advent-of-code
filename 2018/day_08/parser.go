package day_08

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Node interface {
	SumMeta() int
	SumIndexed() int
}

type node struct {
	meta     []int
	children []*node
}

func (n *node) SumMeta() (sum int) {
	sum = 0
	for _, c := range n.children {
		sum += c.SumMeta()
	}
	for _, m := range n.meta {
		sum += m
	}
	return
}

func (n *node) SumIndexed() (sum int) {
	sum = 0
	if len(n.children) == 0 {
		for _, m := range n.meta {
			sum += m
		}
	} else {
		for _, m := range n.meta {
			if m-1 < len(n.children) {
				sum += n.children[m-1].SumIndexed()
			}
		}
	}
	return
}

func newNode() (n *node) {
	n = &node{[]int{}, []*node{}}
	return
}

func tree(depth int, entries []int) (n *node, remainder []int) {
	childCount := entries[0]
	metaCount := entries[1]
	n = newNode()
	remainder = entries[2:]
	var c *node
	for i := 0; i < childCount; i += 1 {
		c, remainder = tree(depth+1, remainder)
		n.children = append(n.children, c)
	}
	for i := 0; i < metaCount; i += 1 {
		n.meta = append(n.meta, remainder[i])
	}
	remainder = remainder[metaCount:]
	return
}

func Parse(path string) *node {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	entries := []int{}
	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		if i, err := strconv.Atoi(scanner.Text()); err == nil {
			entries = append(entries, i)
		} else {
			fmt.Println(err.Error())
			os.Exit(2)
		}
	}

	t, _ := tree(0, entries)
	return t
}
