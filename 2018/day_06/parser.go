package day_06

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

const INFINITE = 0xffffffff

type Area interface {
	IsInfinite() bool
	Size() int
	Number() int
	X() int
	Y() int
	SetSize(size int)
}

type Space interface {
	AddPoint(number int, x, y string) error
	Areas() []Area
	Dimensions() (width int, height int)
}

type point struct {
	number int
	x      int
	y      int
}

func newPoint(number int, x, y string) (p point, e error) {
	xi, xe := strconv.Atoi(x)
	yi, ye := strconv.Atoi(y)
	if xe != nil || ye != nil {
		e = errors.New("Could not convert " + x + " or " + y)
	} else {
		p = point{number, xi, yi}
	}
	return
}

type area struct {
	p    point
	size int
}

func newArea(number int, x, y string) (a *area, e error) {
	p, e := newPoint(number, x, y)
	if e == nil {
		a = &area{p, 0}
	}
	return
}

func (a *area) IsInfinite() bool {
	return a.Size() == INFINITE
}

func (a *area) Size() int {
	return a.size
}

func (a *area) Number() int {
	return a.p.number
}

func (a *area) X() int {
	return a.p.x
}

func (a *area) Y() int {
	return a.p.y
}

func (a *area) SetSize(size int) {
	a.size = size
}

type space struct {
	areas []*area
	clean bool
}

func NewSpace() *space {
	return &space{[]*area{}, false}
}

func (s *space) AddPoint(number int, x, y string) (e error) {
	s.clean = false
	a, e := newArea(number, x, y)
	if e == nil {
		s.areas = append(s.areas, a)
	}
	return
}

func (s *space) Areas() (areas []Area) {
	areas = []Area{}
	for _, a := range s.areas {
		areas = append(areas, a)
	}
	return
}

func (s *space) Dimensions() (width int, height int) {
	width = 0
	height = 0
	for _, a := range s.areas {
		width = max(width, a.p.x)
		height = max(height, a.p.y)
	}
	return
}

func Parse(path string) Space {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	coords := regexp.MustCompile(`^(\d+),\s+(\d+)$`)

	i := 0
	s := NewSpace()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		i += 1

		if tokens := coords.FindStringSubmatch(line); tokens != nil {
			if e := s.AddPoint(i, tokens[1], tokens[2]); e != nil {
				fmt.Println(e)
				os.Exit(9)
			}
		} else if err == nil {
			continue
		} else if err == io.EOF {
			break
		} else {
			fmt.Println("Something bad happened while trying to read the runes.")
			os.Exit(2)
		}
	}
	return s
}
