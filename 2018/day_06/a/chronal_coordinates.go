package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_06"
)

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

type coord struct {
	distance int
	first    int
	second   int
}

func calcSizes(s day_06.Space) {
	width, height := s.Dimensions()

	fabric := make([][]coord, height+1)
	for row := 0; row < len(fabric); row += 1 {
		fabric[row] = make([]coord, width+1)
		for col := 0; col < len(fabric[row]); col += 1 {
			fabric[row][col].distance = day_06.INFINITE
		}
	}

	for row := 0; row < len(fabric); row += 1 {
		for col := 0; col < len(fabric[row]); col += 1 {
			for _, a := range s.Areas() {
				c := fabric[row][col]
				dist := abs(col-a.X()) + abs(row-a.Y())
				if dist < c.distance {
					c.distance = dist
					c.first = a.Number()
					c.second = 0
				} else if dist == c.distance {
					c.second = a.Number()
				}
				fabric[row][col] = c
			}
		}
	}

	for _, row := range []int{0, len(fabric) - 1} {
		for col := 0; col < len(fabric[0]); col += 1 {
			c := fabric[row][col]
			s.Areas()[c.first-1].SetSize(day_06.INFINITE)
		}
	}

	for row := 0; row < len(fabric); row += 1 {
		for _, col := range []int{0, len(fabric[0]) - 1} {
			c := fabric[row][col]
			a := s.Areas()[c.first-1]
			a.SetSize(day_06.INFINITE)
		}
	}

	for _, a := range s.Areas() {
		for row := 0; row < len(fabric); row += 1 {
			for col := 0; col < len(fabric[row]); col += 1 {
				c := fabric[row][col]
				if !a.IsInfinite() && c.second == 0 && c.first == a.Number() {
					a.SetSize(a.Size() + 1)
				}
			}
		}
	}
}

func main() {
	s := day_06.Parse("../input.txt")
	calcSizes(s)
	maxSize := 0
	for _, a := range s.Areas() {
		if !a.IsInfinite() && a.Size() > maxSize {
			maxSize = a.Size()
		}
	}
	fmt.Println(maxSize)
}
