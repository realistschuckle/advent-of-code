package day_05

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"unicode"
)

func canReduce(units []rune) bool {
	if len(units) == 1 {
		return false
	}
	a := unicode.ToLower(units[0])
	b := unicode.ToLower(units[1])
	return a == b && units[0] != units[1]
}

type Polymer interface {
	Reduce() Polymer
	Length() int
	DistinctRunes() []rune
	Filter(r rune) Polymer
}

type polymer struct {
	units []rune
}

func newPolymer() *polymer {
	return &polymer{[]rune{}}
}

func (p *polymer) Reduce() Polymer {
	clone := newPolymer()

	for {
		for i := 0; i < len(p.units); i += 1 {
			if canReduce(p.units[i:]) {
				i += 1
			} else {
				clone.append(p.units[i])
			}
		}

		if len(clone.units) == len(p.units) {
			break
		} else {
			p = clone
			clone = newPolymer()
		}
	}

	return clone
}

func (p *polymer) Length() int {
	return len(p.units)
}

func (p *polymer) DistinctRunes() []rune {
	runeMap := make(map[rune]int)
	for _, r := range p.units {
		runeMap[r] = 1
	}

	distinctRunes := []rune{}
	for r, _ := range runeMap {
		distinctRunes = append(distinctRunes, r)
	}
	return distinctRunes
}

func (p *polymer) Filter(filter rune) Polymer {
	filtered := newPolymer()
	for _, r := range p.units {
		if r != filter && unicode.ToLower(r) != filter {
			filtered.append(r)
		}
	}
	return filtered
}

func (p *polymer) append(r rune) {
	p.units = append(p.units, r)
}

func Parse(path string) (p Polymer) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	polymer := newPolymer()
	reader := bufio.NewReader(f)
	for {
		r, _, err := reader.ReadRune()
		if err == nil && unicode.IsLetter(r) {
			polymer.append(r)
		} else if err == nil {
			continue
		} else if err == io.EOF {
			break
		} else {
			fmt.Println("Something bad happened while trying to read the runes.")
			os.Exit(2)
		}
	}
	return polymer
}
