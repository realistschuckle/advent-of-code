package day_04

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"time"
)

type rawEntry struct {
	When     time.Time
	IsOnDuty bool
	Id       int
}

type byWhen []rawEntry

func (a byWhen) Len() int           { return len(a) }
func (a byWhen) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byWhen) Less(i, j int) bool { return a[i].When.Before(a[j].When) }

const layout = "2006-01-02 15:04"

func newRawEntry(timestamp string, isOnDuty bool, id int) (entry rawEntry) {
	t, _ := time.Parse(layout, timestamp)
	entry = rawEntry{t, isOnDuty, id}
	return
}

type SleepRecord struct {
	minutes []int
}

func newSleepRecord() (record *SleepRecord) {
	record = &SleepRecord{make([]int, 60)}
	return
}

func (s *SleepRecord) addInterval(from int, to int) {
	for i := from; i < to; i += 1 {
		s.minutes[i] += 1
	}
}

func (s *SleepRecord) TotalMinutesSlept() (sum int) {
	sum = 0
	for i := 0; i < len(s.minutes); i += 1 {
		sum += s.minutes[i]
	}
	return
}

func (s *SleepRecord) MostSleptMinute() (max int, amount int) {
	amount = 0
	for i := 0; i < len(s.minutes); i += 1 {
		if s.minutes[i] > amount {
			max = i
			amount = s.minutes[i]
		}
	}
	return
}

func Parse(path string) (records map[int]*SleepRecord) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	time := regexp.MustCompile(`^\[(\d+-\d+-\d+ \d+:\d+)\]`)
	guardId := regexp.MustCompile(`Guard #(\d+)`)

	entries := []rawEntry{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		isOnDuty := false
		id := 0
		timestamp := ""
		if tokens := time.FindStringSubmatch(line); tokens != nil {
			timestamp = tokens[1]
			if tokens = guardId.FindStringSubmatch(line); tokens != nil {
				if id, err = strconv.Atoi(tokens[1]); err != nil {
					fmt.Println("Could not convert " + tokens[1] + " to an integer")
					os.Exit(2)
				}
				isOnDuty = true
			}
		} else {
			fmt.Println("Could not find timestamp in " + line)
			os.Exit(3)
		}
		entries = append(entries, newRawEntry(timestamp, isOnDuty, id))
	}
	sort.Sort(byWhen(entries))

	records = make(map[int]*SleepRecord)

	for i := 0; i < len(entries); i += 1 {
		entry := entries[i]
		if entry.IsOnDuty {
			for i+1 < len(entries) && !entries[i+1].IsOnDuty {
				sleep := entries[i+1]
				wake := entries[i+2]

				if records[entry.Id] == nil {
					records[entry.Id] = newSleepRecord()
				}
				records[entry.Id].addInterval(sleep.When.Minute(), wake.When.Minute())
				i += 2
			}
		}
	}
	return
}
