package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_02"
)

func main() {
	lines := day_02.Parse("../input.txt")
	twos := 0
	threes := 0
	for _, line := range lines {
		letters := make(map[rune]int)
		for _, r := range line {
			letters[r] += 1
		}
		two := 0
		three := 0
		for _, v := range letters {
			if v == 2 {
				two += 1
			}
			if v == 3 {
				three += 1
			}
		}
		if two >= 1 {
			twos += 1
		}
		if three >= 1 {
			threes += 1
		}
	}
	fmt.Println(twos * threes)
}
