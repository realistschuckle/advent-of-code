package day_10

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type point struct {
	x  int
	y  int
	dx int
	dy int
}

func (p *point) advance() {
	p.x += p.dx
	p.y += p.dy
}
func (p *point) unadvance() {
	p.x -= p.dx
	p.y -= p.dy
}

type Sky struct {
	coords         []*point
	previousHeight int
}

func (s *Sky) newHeight() int {
	minY := 1<<31 - 1
	maxY := -1 << 31

	for _, coord := range s.coords {
		if coord.y > maxY {
			maxY = coord.y
		}
		if coord.y < minY {
			minY = coord.y
		}
	}
	return maxY - minY
}

func (s *Sky) AddCoord(x string, y string, dx string, dy string) (err error) {
	var ax, ay, adx, ady int
	x = strings.TrimSpace(x)
	y = strings.TrimSpace(y)
	dx = strings.TrimSpace(dx)
	dy = strings.TrimSpace(dy)
	if ax, err = strconv.Atoi(x); err == nil {
		if ay, err = strconv.Atoi(y); err == nil {
			if adx, err = strconv.Atoi(dx); err == nil {
				if ady, err = strconv.Atoi(dy); err == nil {
					s.coords = append(s.coords, &point{ax, ay, adx, ady})
					s.previousHeight = s.newHeight()
				}
			}
		}
	}
	return
}

func (s *Sky) Tick() (contracted bool) {
	height := s.previousHeight
	for _, coord := range s.coords {
		coord.advance()
	}
	newHeight := s.newHeight()
	contracted = newHeight < height
	s.previousHeight = newHeight
	return
}

func (s *Sky) Untick() {
	for _, coord := range s.coords {
		coord.unadvance()
	}
}

func (s *Sky) Print() {
	minX := 1<<31 - 1
	maxX := -1 << 31
	minY := 1<<31 - 1
	maxY := -1 << 31

	for _, coord := range s.coords {
		if coord.x > maxX {
			maxX = coord.x
		}
		if coord.x < minX {
			minX = coord.x
		}
		if coord.y > maxY {
			maxY = coord.y
		}
		if coord.y < minY {
			minY = coord.y
		}
	}

	for j := minY; j <= maxY; j += 1 {
		for i := minX; i <= maxX; i += 1 {
			hasCoord := false
			for _, coord := range s.coords {
				if coord.x == i && coord.y == j {
					hasCoord = true
					break
				}
			}
			if hasCoord {
				fmt.Print("#")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

func Parse(path string) (sky *Sky) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not find file '" + path + "'")
		os.Exit(1)
	}
	defer f.Close()

	coord := regexp.MustCompile(`^position=<([ -]\d+), ([ -]\d+)> velocity=<([ -]\d+), ([ -]\d+)>$`)

	sky = &Sky{[]*point{}, 0}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if tokens := coord.FindStringSubmatch(line); tokens != nil {
			if e := sky.AddCoord(tokens[1], tokens[2], tokens[3], tokens[4]); e != nil {
				fmt.Println(e)
				os.Exit(9)
			}
		} else if err == nil {
			continue
		} else if err == io.EOF {
			break
		} else {
			fmt.Println("Something bad happened while trying to read the runes.")
			os.Exit(2)
		}
	}
	return
}
