package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_10"
)

func main() {
	sky := day_10.Parse("../input.txt")
	i := 1
	for sky.Tick() {
		i += 1
	}
	fmt.Println(i - 1)
}
