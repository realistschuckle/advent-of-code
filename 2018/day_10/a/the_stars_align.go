package main

import (
	"gitlab.com/realistschuckle/advent-of-code-2018/day_10"
)

func main() {
	sky := day_10.Parse("../input.txt")
	for sky.Tick() {
	}
	sky.Untick()
	sky.Print()
}
