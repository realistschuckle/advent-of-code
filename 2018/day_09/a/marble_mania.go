package main

import (
	"fmt"

	"gitlab.com/realistschuckle/advent-of-code-2018/day_09"
)

func main() {
	conds := day_09.Parse("../input.txt")
	for _, cond := range conds {
		playerScores := make([]int, cond.NumberOfPlayers)
		cq := day_09.NewCircularQueue()
		for i := 1; i < cond.LastMarblePlayed+1; i += 1 {
			day_09.Play(&cq, i, playerScores)
		}

		highScore := 0
		for _, score := range playerScores {
			if score > highScore {
				highScore = score
			}
		}
		fmt.Println(highScore)
		if cond.HasExpectedAnswer && highScore == cond.ExpectedAnswer {
			fmt.Println("That is correct")
		} else if cond.HasExpectedAnswer && highScore != cond.ExpectedAnswer {
			fmt.Println(playerScores, "Was looking for", cond.ExpectedAnswer)
		}
	}
}
