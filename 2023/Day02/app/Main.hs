module Main (main) where

import Lib (part1, part2)

main :: IO ()
main = do
  contents <- getContents
  print $ part1 . lines $ contents
  print $ part2 . lines $ contents
