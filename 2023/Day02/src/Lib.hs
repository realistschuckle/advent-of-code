module Lib (
  part1,
  part2,
) where

import Data.Char (
  digitToInt,
  isDigit,
  isPunctuation,
  isSeparator,
 )
import Data.Maybe (fromMaybe)

data GameSummary = GameSummary
  { game :: Game
  , maxRed :: Int
  , maxGreen :: Int
  , maxBlue :: Int
  }

data Try = Try
  { red :: Maybe Int
  , green :: Maybe Int
  , blue :: Maybe Int
  }
  deriving (Show)

data Game = Game
  { gameId :: Int
  , tries :: [Try]
  }
  deriving (Show)

data CubeCount
  = Red Int
  | Blue Int
  | Green Int
  | None
  deriving (Show)

part1 :: [String] -> Int
part1 records = sum validGameSummaryIds
 where
  part1Games = map calcGameSummary records
  validGameSummaries = filter validGameSummary part1Games
  validGameSummaryIds = map (gameId . game) validGameSummaries

part2 :: [String] -> Int
part2 records = sum powerCubes
 where
  part2Games = map calcGameSummary records
  powerCubes = map calcPowerCube part2Games

calcPowerCube :: GameSummary -> Int
calcPowerCube g = maxRed g * maxGreen g * maxBlue g

validGameSummary :: GameSummary -> Bool
validGameSummary g = (maxRed g < 13) && (maxGreen g < 14) && (maxBlue g < 15)

calcGameSummary :: String -> GameSummary
calcGameSummary s = GameSummary game' r g b
 where
  game' = parseGame s
  r = maxColor red (tries game')
  g = maxColor green (tries game')
  b = maxColor blue (tries game')

maxColor :: (Try -> Maybe Int) -> [Try] -> Int
maxColor f ts = foldl max 0 (map (fromMaybe 0 . f) ts)

parseGame :: String -> Game
parseGame ('G' : 'a' : 'm' : 'e' : ' ' : cs) = Game id' ts
 where
  (id', s) = parseNumber cs
  ts = parseTries s
parseGame s = error ("Could not parse game " ++ s)

parseTries :: String -> [Try]
parseTries "" = []
parseTries s = t : parseTries s'
 where
  (t, s') = parseTry s

parseTry :: String -> (Try, String)
parseTry = parseTry' False (Try Nothing Nothing Nothing)
 where
  parseTry' :: Bool -> Try -> String -> (Try, String)
  parseTry' _ t "" = (t, "")
  parseTry' False t (';' : ' ' : cs) = parseTry' True (foldTry t cubeCount) s'
   where
    (cubeCount, s') = parseCubeCount cs
  parseTry' True t (';' : ' ' : cs) = (t, ';' : ' ' : cs)
  parseTry' _ t (':' : ' ' : cs) = parseTry' True (foldTry t cubeCount) s'
   where
    (cubeCount, s') = parseCubeCount cs
  parseTry' _ t (',' : ' ' : cs) = parseTry' True (foldTry t cubeCount) s'
   where
    (cubeCount, s') = parseCubeCount cs
  parseTry' _ _ s = error ("Cannot parse try from " ++ s)

foldTry :: Try -> CubeCount -> Try
foldTry t' cc =
  case cc of
    Red r -> t'{red = Just r}
    Green g -> t'{green = Just g}
    Blue b -> t'{blue = Just b}
    None -> error "Not a cube count and everything is wrong"

parseCubeCount :: String -> (CubeCount, String)
parseCubeCount s = parseCubeCount' num (ignore s')
 where
  (num, s') = parseNumber s
  parseCubeCount' :: Int -> String -> (CubeCount, String)
  parseCubeCount' n ('b' : 'l' : 'u' : 'e' : cs) = (Blue n, cs)
  parseCubeCount' n ('r' : 'e' : 'd' : cs) = (Red n, cs)
  parseCubeCount' n ('g' : 'r' : 'e' : 'e' : 'n' : cs) = (Green n, cs)
  parseCubeCount' _ s'' = error ("Could not parse cube count " ++ s'')

parseNumber :: String -> (Int, String)
parseNumber s = parseNumber' s 0
 where
  parseNumber' :: String -> Int -> (Int, String)
  parseNumber' "" n = (n, "")
  parseNumber' (c : cs) n
    | isDigit c = parseNumber' cs (10 * n + digitToInt c)
    | otherwise = (n, c : cs)

ignore :: String -> String
ignore = ignore'
 where
  ignore' :: String -> String
  ignore' "" = ""
  ignore' (c : cs)
    | isPunctuation c = ignore cs
    | isSeparator c = ignore cs
    | otherwise = c : cs
