module Lib (
  part1,
  part2,
  text2num,
) where

import Data.Bifunctor (bimap)
import Data.Char (digitToInt, isAlpha, isDigit)
import Data.Maybe (listToMaybe)
import qualified Data.Text as T

part1 :: String -> Maybe Int
part1 s = (+) <$> tens <*> ones
 where
  nums = map digitToInt $ filter (not . isAlpha) s
  tens = (* 10) <$> listToMaybe nums
  ones = listToMaybe (reverse nums)

part2 :: String -> Maybe Int
part2 = part1 . text2num

nummap :: [(T.Text, T.Text)]
nummap =
  map
    (bimap T.pack T.pack)
    [ ("one", "1")
    , ("two", "2")
    , ("three", "3")
    , ("four", "4")
    , ("five", "5")
    , ("six", "6")
    , ("seven", "7")
    , ("eight", "8")
    , ("nine", "9")
    ]

getDigitOrEmpty :: T.Text -> T.Text
getDigitOrEmpty t =
  case T.uncons t of
    Nothing -> T.empty
    Just (c, _) ->
      if isDigit c
        then T.singleton c
        else T.empty

wordToDigitOrEmpty :: T.Text -> T.Text -> T.Text -> T.Text
wordToDigitOrEmpty x a b =
  if a `T.isPrefixOf` x
    then b
    else getDigitOrEmpty x

text2num :: String -> String
text2num s = T.unpack . T.concat $ tuples
 where
  tails_ = T.tails . T.pack $ s
  tuples = [wordToDigitOrEmpty x a b | x <- tails_, (a, b) <- nummap]

-- text2num "" = ""
-- text2num ('o' : 'n' : 'e' : xs) = '1' : text2num ('e' : xs)
-- text2num ('t' : 'w' : 'o' : xs) = '2' : text2num ('o' : xs)
-- text2num ('t' : 'h' : 'r' : 'e' : 'e' : xs) = '3' : text2num ('e' : xs)
-- text2num ('f' : 'o' : 'u' : 'r' : xs) = '4' : text2num xs
-- text2num ('f' : 'i' : 'v' : 'e' : xs) = '5' : text2num ('e' : xs)
-- text2num ('s' : 'i' : 'x' : xs) = '6' : text2num xs
-- text2num ('s' : 'e' : 'v' : 'e' : 'n' : xs) = '7' : text2num ('n' : xs)
-- text2num ('e' : 'i' : 'g' : 'h' : 't' : xs) = '8' : text2num ('t' : xs)
-- text2num ('n' : 'i' : 'n' : 'e' : xs) = '9' : text2num ('e' : xs)
-- text2num (x : xs) = x : text2num xs
