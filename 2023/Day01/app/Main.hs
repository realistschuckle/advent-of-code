module Main (main) where

import Lib (part1, part2)

main :: IO ()
main = do
  contents <- getContents
  print $ sum <$> mapM part1 (lines contents)
  print $ sum <$> mapM part2 (lines contents)
