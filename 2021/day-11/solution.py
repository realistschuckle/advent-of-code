from typing import Callable

Input = list[list[int]]


def parse(line_no: int, line: int) -> Input:
    return [int(x) for x in line if len(x.strip())]


def read_data(data_file: str, fn: Callable[[int, int], Input]) -> Input:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


def flash(input: Input, row_index: int, col_index: int):
    if input[row_index][col_index] == 10:
        if row_index > 0:
            input[row_index - 1][col_index] += 1
            flash(input, row_index - 1, col_index)

        if row_index < len(input) - 1:
            input[row_index + 1][col_index] += 1
            flash(input, row_index + 1, col_index)

        if col_index > 0:
            input[row_index][col_index - 1] += 1
            flash(input, row_index, col_index - 1)

        if col_index < len(input[row_index]) - 1:
            input[row_index][col_index + 1] += 1
            flash(input, row_index, col_index + 1)

        if row_index > 0 and col_index > 0:
            input[row_index - 1][col_index - 1] += 1
            flash(input, row_index - 1, col_index - 1)

        if row_index > 0 and col_index < len(input[row_index]) - 1:
            input[row_index - 1][col_index + 1] += 1
            flash(input, row_index - 1, col_index + 1)

        if row_index < len(input) - 1 and col_index > 0:
            input[row_index + 1][col_index - 1] += 1
            flash(input, row_index + 1, col_index - 1)

        if (
            row_index < len(input) - 1
            and col_index < len(input[row_index]) - 1
        ):
            input[row_index + 1][col_index + 1] += 1
            flash(input, row_index + 1, col_index + 1)


def solution(input: Input, days: int) -> int:
    day_100_flashes = 0
    first_day_flash = -1
    flashes = 0
    day = 0
    while first_day_flash < 0 or day <= 100:
        day += 1
        last_flashes = flashes
        for row_index in range(len(input)):
            for col_index in range(len(input[row_index])):
                input[row_index][col_index] += 1
                flash(input, row_index, col_index)
        for row_index in range(len(input)):
            for col_index in range(len(input[row_index])):
                if input[row_index][col_index] > 9:
                    flashes += 1
                    input[row_index][col_index] = 0
        if day == 100:
            day_100_flashes = flashes
        if flashes == last_flashes + len(input) * len(input[0]):
            first_day_flash = day
    return day_100_flashes, first_day_flash


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data, 100))
