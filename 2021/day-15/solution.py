import re
from typing import Callable

Line = list[int]
Input = list[Line]
Parser = Callable[[int, str], Line]


def parseList(*converters, sep=",", test=None) -> Parser:
    def parser(line_no: int, line: str) -> Line:
        pieces = line
        if sep is not None:
            pieces = re.compile(sep).split(line)
        return [
            converters[i](x)
            for i, x in enumerate(pieces)
            if converters[i] is not None
            and (test is None or test(line_no, line))
        ]

    return parser


def parseIntList(len_):
    return parseList(*[int] * len_)


def read_data(data_file: str, fn: Parser) -> Input:
    with open(data_file) as f:
        parsed_lines = [
            fn(i, line.strip()) for i, line in enumerate(f) if line
        ]
    return [line for line in parsed_lines if line]


def fix_up(data: Input, totals: Input, x: int, y: int):
    if y > 0 and totals[y][x] + data[y - 1][x] < totals[y - 1][x]:
        totals[y - 1][x] = totals[y][x] + data[y - 1][x]
        fix_up(data, totals, x, y - 1)
        for dx in range(x, 1, -1):
            if totals[y - 1][dx] + data[y - 1][dx - 1] < totals[y - 1][dx - 1]:
                totals[y - 1][dx - 1] = totals[y - 1][dx] + data[y - 1][dx - 1]
            if (
                y > 1
                and totals[y - 1][dx - 1]
                > totals[y - 2][dx - 1] + data[y - 1][dx - 1]
            ):
                totals[y - 1][dx - 1] = (
                    totals[y - 1][dx - 1] + data[y - 1][dx - 1]
                )
        for dx in range(x, len(data) - 1):
            if totals[y - 1][dx] + data[y - 1][dx + 1] < totals[y - 1][dx + 1]:
                totals[y - 1][dx + 1] = totals[y - 1][dx] + data[y - 1][dx + 1]
            if (
                y > 1
                and totals[y - 1][dx + 1]
                > totals[y - 2][dx + 1] + data[y - 1][dx + 1]
            ):
                totals[y - 1][dx + 1] = (
                    totals[y - 2][dx + 1] + data[y - 1][dx + 1]
                )


def fix_left(data: Input, totals: Input, x: int, y: int):
    if x > 0 and totals[y][x] + data[y][x - 1] < totals[y][x - 1]:
        totals[y][x - 1] = totals[y][x] + data[y][x - 1]
        fix_left(data, totals, x - 1, y)
        for dy in range(y, 1, -1):
            if totals[dy][x - 1] + data[dy - 1][x - 1] < totals[dy - 1][x - 1]:
                totals[dy - 1][x - 1] = totals[dy][x - 1] + data[dy - 1][x - 1]
            if (
                x > 1
                and totals[dy - 1][x - 1]
                > totals[dy - 1][x - 2] + data[dy - 1][x - 1]
            ):
                totals[dy - 1][x - 1] = (
                    totals[dy - 1][x - 1] + data[dy - 1][x - 1]
                )
        for dy in range(y, len(data) - 1):
            if totals[dy][x - 1] + data[dy + 1][x - 1] < totals[dy + 1][x - 1]:
                totals[dy + 1][x - 1] = totals[dy][x - 1] + data[dy + 1][x - 1]
            if (
                x > 1
                and totals[dy + 1][x - 1]
                > totals[dy + 1][x - 2] + data[dy + 1][x - 1]
            ):
                totals[dy + 1][x - 1] = (
                    totals[dy + 1][x - 2] + data[dy + 1][x - 1]
                )


def solution(data: Input) -> int:
    """
    Solves this problem by creating an array for totals, so this is kind
    of dynamic programming. Here's how it works. For each entry x, y in the
    table, calculate set the corresponding total as the minimum of the two
    sums:
      * entry[x, y] + totals[x-1, y]
      * entry[x, y] + totals[x, y-1]
    If the two sums are equal, then just set the value. If they're not
    equal, then set the value to the minimum and propogate the change in
    the direction from which the min values was set, either left or up.
    The propogation updates totals based on the new totals, if they'return
    less than the one already there. This allows the algorithm to fix the
    table since its calculation loop runs left-to-right, top-to-bottom, the
    opportunity to fix left and fix up allows the changes to propogate in
    directions counter to the main calculation loop.
    """
    size = len(data)
    totals = [[0] * size for row in data]
    for x in range(1, size):
        totals[0][x] = totals[0][x - 1] + data[0][x]
    for y in range(1, size):
        totals[y][0] = totals[y - 1][0] + data[y][0]
        for x in range(1, size):
            left_total = totals[y][x - 1] + data[y][x]
            top_total = totals[y - 1][x] + data[y][x]
            if left_total < top_total:
                totals[y][x] = left_total
                fix_up(data, totals, x, y)
            elif top_total < left_total:
                totals[y][x] = top_total
                fix_left(data, totals, x, y)
            else:
                totals[y][x] = top_total
    return totals[-1][-1]


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, lambda _, line: [int(x) for x in line])
        print(solution(data))

        new_data = []
        for y, row in enumerate(data):
            new_row = []
            for i in range(5):
                for x, col in enumerate(row):
                    value = col + i if col + i < 10 else (col + i) % 10 + 1
                    new_row.append(value)
            new_data.append(new_row)
        for i in range(len(new_data) * 4):
            new_row = []
            for c in new_data[i]:
                value = c + 1 if c + 1 < 10 else 1
                new_row.append(value)
            new_data.append(new_row)
        print(solution(new_data))
