def all_pairs_shortest_path(costs, k):
    for i in range(len(costs)):
        for j in range(len(costs)):
            costs[i][j] = min(costs[i][j], costs[i][k] + costs[k][j])


def print_costs_as_latex(costs, k):
    align_spec = "c|" + "c" * len(costs)
    print("\\begin{table}[H]")
    print("\\begin{center}")
    print("\\caption{", end="")
    if k == 0:
        print("\\( A^0(i, j) = cost(i, j) \\)", end="")
    else:
        print(
            "\\( A^%d(i, j) = \\min \\{ A^%d(i, j), A^%d(i, %d) + A^%d(%d, j) \\} \\)"
            % (k, k - 1, k - 1, k, k - 1, k),
            end="",
        )
    print("}\n")
    print("\\begin{tabular}{%s}" % align_spec)
    headers = " & ".join([str(i + 1) for i in range(len(costs))])
    print("$A^%d$ & %s \\\\" % (k, headers))
    print("\hline")
    for i in range(len(costs)):
        print(i + 1, end=" ")
        for j in range(len(costs)):
            value = costs[i][j] if costs[i][j] < float("inf") else "$\\infty$"
            print(f"& {value}", end=" ")
        print("\\\\")
    print("\\end{tabular}")
    print("\\end{center}")
    print("\\end{table}")


if __name__ == "__main__":
    i = float("inf")
    costs = [
        [0, 3, 2, 5, i, i],
        [3, 0, i, 1, 4, i],
        [2, i, 0, 2, i, 1],
        [5, 1, 2, 0, 3, i],
        [i, 4, i, 3, 0, 2],
        [i, i, 1, i, 2, 0],
    ]
    print_costs_as_latex(costs, 0)
    for k in range(len(costs)):
        all_pairs_shortest_path(costs, k)
        print_costs_as_latex(costs, k + 1)
