from pprint import pprint
import re
from typing import Callable

Line = list[int]
Input = list[Line]
LineParser = Callable[[int, str], Line]
Parser = Callable[[int, str], Input]


def parseList(*converters, sep=",", test=None, verbose=False) -> LineParser:
    def parser(line_no: int, line: str) -> Line:
        pieces = line
        if sep is not None:
            pieces = re.compile(sep).split(line)
        if verbose:
            print("PIECES")
            pprint(pieces)
        return [
            converters[i](x)
            for i, x in enumerate(pieces)
            if converters[i] is not None
            and (test is None or test(line_no, line))
        ]

    return parser


def parse(line_no: int, line: str, verbose=False) -> Line:
    return line.strip()


def read_data(data_file: str, fn: Parser, verbose=False) -> Input:
    with open(data_file) as f:
        return [
            fn(i, line, verbose=verbose)
            for i, line in enumerate(f)
            if line[0] != "#"
        ]


class Window:
    def __init__(self, bits):
        self._bits = bits
        self._index = 0

    def get_constant_group(self):
        indicator = self._fromBits(5)
        return indicator[0], indicator[1:]

    def get_number_of_packets(self):
        return self._fromBits(11)

    def get_op_type(self):
        return self._fromBits(1)

    def get_packet_bit_length(self):
        return self._fromBits(15)

    def get_type(self):
        return self._fromBits(3)

    def get_version(self):
        return self._fromBits(3)

    def get_window(self, number_of_bits):
        s = self._fromBits(number_of_bits)
        return Window(s)

    def go_to_boundary(self):
        while self._index % 4 != 0:
            self._index += 1

    @property
    def index(self):
        return self._index

    def _fromBits(self, len_):
        start = self._index
        end = self._index + len_
        if start > len(self._bits) or end > len(self._bits):
            raise IndexError()
        self._index = end
        return self._bits[start:end]


class Source:
    def __init__(self, content):
        self._content = content
        self._index = 0
        self._buffer = ""

    def get_constant_group(self):
        indicator = self._fromBuffer(5)
        return indicator[0], indicator[1:]

    def get_number_of_packets(self):
        return self._fromBuffer(11)

    def get_op_type(self):
        return self._fromBuffer(1)

    def get_packet_bit_length(self):
        return self._fromBuffer(15)

    def get_type(self):
        return self._fromBuffer(3)

    def get_version(self):
        return self._fromBuffer(3)

    def get_window(self, number_of_bits):
        s = self._fromBuffer(number_of_bits)
        return Window(s)

    def go_to_boundary(self):
        while self._index % 4 != 0:
            self._index += 1

    @property
    def index(self):
        return self._index

    def _fromBuffer(self, len_):
        while len(self._buffer) < len_:
            self._next_hex()
        content = self._buffer[0:len_]
        self._buffer = self._buffer[len_:]
        return content

    def _next_hex(self):
        for _ in range(2):
            hex_ = self._content[self._index]
            byte = bin(int(hex_, base=16))[2:]
            self._index += 1
            while len(byte) < 4:
                byte = "0" + byte
            self._buffer += byte

    def __str__(self):
        remaining = self._content[self._index :]
        return (
            f"<Source index:{self._index} buffer:{self._buffer}\n"
            f"  remaining:{remaining if remaining else '<empty>'}>"
        )


def parse_packets(source: Source, verbose=False):
    packet_start = source.index
    version = int(source.get_version(), base=2)
    type_ = int(source.get_type(), base=2)
    if verbose:
        print("=" * 40)
        print("Source index", packet_start)
        print("version ", version)
        print("type    ", type_)
    if type_ == 4:
        constant = ""
        while True:
            i, value = source.get_constant_group()
            constant += value
            if i == "0":
                constant = int(constant, base=2)
                if verbose:
                    print("constant", constant)
                # source.go_to_boundary()
                return version, type_, constant
    else:
        op_type = int(source.get_op_type(), base=2)
        if op_type == 0:
            packet_bit_length_source = source.get_packet_bit_length()
            packet_bit_length = int(packet_bit_length_source, base=2)
            if verbose:
                print(
                    "packets in",
                    packet_bit_length,
                    "bits (",
                    packet_bit_length_source,
                    ")",
                )
            packets = []
            window = source.get_window(packet_bit_length)
            while True:
                try:
                    if verbose:
                        print(f"source: {window._bits}")
                    packets.append(parse_packets(window, verbose))
                except IndexError:
                    break
            return version, type_, op_type, packet_bit_length, packets
        elif op_type == 1:
            number_of_packets = int(source.get_number_of_packets(), base=2)
            if verbose:
                print(number_of_packets, "packets")
            packets = []
            current_index = source.index
            for i in range(number_of_packets):
                try:
                    packets.append(parse_packets(source, verbose))
                except IndexError:
                    break
            if len(packets) != number_of_packets:
                if verbose:
                    print(version, type_, op_type, number_of_packets)
                    pprint(packets)
                msg = (
                    f"Expected {number_of_packets} but got {len(packets)}\n"
                    f"packet start: {packet_start}, "
                    f"column index: {current_index}\n"
                    f"{source}"
                )
                raise RuntimeError(msg)
            return version, type_, op_type, number_of_packets, packets
        else:
            raise RuntimeError("CAN'T PARSE!")


def sum_visitor(tree, verbose=False):
    if tree is None:
        return 0
    sum = tree[0]
    if len(tree) > 3:
        for branch in tree[-1]:
            sum += sum_visitor(branch)
    return sum


def evaluate(indent, tree, verbose=False):
    if verbose:
        print("  " * indent, end="")
        if len(tree) > 3:
            print("current packet", tree[0:-1], len(tree[-1]))
        else:
            print("current packet", tree)
    op_type = tree[1]
    if op_type == 0:
        sum_ = 0
        if verbose:
            print(
                "  " * (indent - 1), " evaluating SUM", len(tree[-1]), "nodes"
            )
        for packet in tree[-1]:
            value = evaluate(indent + 2, packet, verbose)
            if verbose:
                print("  " * (indent - 1), " SUM", sum_, "+", value)
            sum_ += value
        if verbose:
            print("  " * (indent - 1), " SUM =", sum_)
        return sum_
    elif op_type == 1:
        product_ = 1
        if verbose:
            print(
                "  " * (indent - 1),
                " evaluating PRODUCT",
                len(tree[-1]),
                "nodes",
            )
        for packet in tree[-1]:
            value = evaluate(indent + 2, packet, verbose)
            if verbose:
                print("  " * (indent - 1), " PRODUCT", product_, "*", value)
            product_ *= value
        return product_
    elif op_type == 2:
        min_ = float("inf")
        if verbose:
            print(
                "  " * (indent - 1), " evaluating MIN", len(tree[-1]), "nodes"
            )
        for packet in tree[-1]:
            value = evaluate(indent + 2, packet, verbose)
            if verbose:
                print("  " * indent, " MIN", min_, ",", value)
            min_ = min(min_, value)
        return min_
    elif op_type == 3:
        max_ = -1
        if verbose:
            print(
                "  " * (indent - 1), " evaluating MAX", len(tree[-1]), "nodes"
            )
        for packet in tree[-1]:
            value = evaluate(indent + 2, packet, verbose)
            if verbose:
                print("  " * (indent - 1), " MAX", max_, ",", value)
            max_ = max(max_, value)
        return max_
    elif op_type == 4:
        return tree[2]
    else:
        left = evaluate(indent + 2, tree[-1][0], verbose)
        right = evaluate(indent + 2, tree[-1][1], verbose)
        if op_type == 5:
            if verbose:
                print("  " * (indent + 1), "evaluating", left, ">", right)
            return 1 if left > right else 0
        elif op_type == 6:
            if verbose:
                print("  " * (indent + 1), "evaluating", left, "<", right)
            return 1 if left < right else 0
        elif op_type == 7:
            if verbose:
                print("  " * (indent + 1), "evaluating", left, "=", right)
            return 1 if left == right else 0


def solution(
    data: Input, verbose_parse=False, verbose1=False, verbose2=False
) -> int:
    sums = []
    for d in data:
        if verbose_parse or verbose1 or verbose2:
            print(" " * 40)
            print("=" * 40)
            print("PARSING", d)
        packet = parse_packets(Source(d), verbose_parse)
        if verbose1:
            print(packet)
        sums.append(
            (sum_visitor(packet, verbose1), evaluate(0, packet, verbose2))
        )
    return sums


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    verbose1 = False
    verbose2 = False
    verbose_parse = False
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--verbose-1" or arg == "-v1":
            verbose1 = True
        if arg == "--verbose-2" or arg == "-v2":
            verbose2 = True
        if arg == "--verbose-parse":
            verbose_parse = True
        elif arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help] [--verbose]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message.
    --verbose-parse  Print data parsing diagnostics
    --verbose-1, -v1 Print diagnostic info for part 1
    --verbose-2, -v2 Print diagnostic info for part 2"""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse, verbose=verbose_parse)
        print(
            solution(
                data,
                verbose_parse=verbose_parse,
                verbose1=verbose1,
                verbose2=verbose2,
            )
        )
