from copy import deepcopy
from functools import reduce
from typing import Callable


Input = list[list[int]]


def parse(line_no: int, line: int) -> Input:
    return [int(x) for x in line if len(x.strip())]


def read_data(data_file: str, fn: Callable[[int, int], Input]) -> Input:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


def low_points(data: Input) -> int:
    sum = 0
    for i, row in enumerate(data):
        for j, col in enumerate(row):
            if (
                (j == 0 or col < data[i][j - 1])
                and (j == len(row) - 1 or col < data[i][j + 1])
                and (i == 0 or col < data[i - 1][j])
                and (i == len(data) - 1 or col < data[i + 1][j])
            ):
                sum += 1 + col
    return sum


def basins(data: Input, show_map=False) -> int:
    data = deepcopy(data)
    for i in range(len(data)):
        for j in range(len(data[0])):
            if data[i][j] != 9:
                data[i][j] = 0
    c = ":"
    for j in range(len(data[0])):
        if data[0][j] == 0:
            data[0][j] = c
        elif data[0][j - 1] != 9:
            c = chr(ord(c) + 1)
            while not c.isalpha():
                c = chr(ord(c) + 1)
    for i in range(len(data)):
        if i == 0:
            continue
        for j in range(len(data[i])):
            if data[i][j] == 0:
                if data[i - 1][j] != 9:
                    data[i][j] = data[i - 1][j]
                    for x in range(j - 1, -1, -1):
                        if data[i][x] == 0:
                            data[i][x] = data[i][j]
                        if data[i][x] == 9:
                            break
                elif data[i][j - 1] != 9:
                    data[i][j] = data[i][j - 1]
            if data[i][j] == 0 and (
                j == len(data[i]) - 1 or data[i][j + 1] == 9
            ):
                c = chr(ord(c) + 1)
                while not c.isalpha():
                    c = chr(ord(c) + 1)
                data[i][j] = c
                for x in range(j - 1, -1, -1):
                    if data[i][x] == 0:
                        data[i][x] = data[i][j]
                    if data[i][x] == 9:
                        break
    for i in range(1, len(data)):
        for j in range(1, len(data[i])):
            a = data[i][j]
            b = data[i][j - 1]
            if a != b and a != 9 and b != 9:
                data = [
                    [c if c != a and c != b else a for c in row]
                    for row in data
                ]
    for i in range(len(data)):
        for j in range(len(data[i])):
            if data[i][j] == 9:
                data[i][j] = "█"
    if show_map:
        for row in data:
            print("".join([str(x) for x in row]))
    counts = {}
    for row in data:
        for col in row:
            if col == 9:
                continue
            if col not in counts:
                counts[col] = 0
            counts[col] += 1
    counts = sorted(
        [item for item in counts.items() if item[0] != "█"], key=lambda x: x[1]
    )
    return reduce(lambda acc, x: acc * x[1], counts[-3:], 1)


if __name__ == "__main__":
    import sys

    show_map = False
    files = ["example.data", "problem.data"]
    if sys.argv[1][0] != "-":
        files = [sys.argv[1]]
    for arg in sys.argv:
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [--help] [--show-map]
    --help, -h       Print this message.
    --show-map, -v   Show the calculated basic map."""
            )
            exit(0)
        if arg == "--show-map" or arg == "-v":
            show_map = True
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        print(low_points(data))
        print(basins(data, show_map=show_map))
