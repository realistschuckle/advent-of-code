import re
from typing import Callable

Line = list[int]
Input = list[Line]
Parser = Callable[[int, str], Line]


def parseList(*converters, sep=",", test=None) -> Parser:
    def parser(line_no: int, line: str) -> Line:
        pieces = line
        if sep is not None:
            pieces = re.compile(sep).split(line)
        return [
            converters[i](x)
            for i, x in enumerate(pieces)
            if converters[i] is not None
            and (test is None or test(line_no, line))
        ]

    return parser


def parse(line_no: int, line: str) -> Line:
    return [int(x) for x in line if len(x.strip())]


def read_data(data_file: str, fn: Callable[[int, str], Input]) -> Input:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


def solution(data: Input) -> int:
    pass


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data))
