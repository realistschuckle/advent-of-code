from typing import Callable

Edge = tuple[str, str]
Input = tuple[dict[str, int], dict[str, int], list[list[int]]]


def parse(line_no: int, line: int) -> list[Edge]:
    return line.strip().split("-")


def read_data(data_file: str, fn: Callable[[int, int], Input]) -> Input:
    with open(data_file) as f:
        edges = [fn(i, line) for i, line in enumerate(f)]
    nodes = {}
    costs = {}
    node_index = 0
    for f, t in edges:
        if f not in nodes:
            nodes[f] = node_index
            node_index += 1
        if t not in nodes:
            nodes[t] = node_index
            node_index += 1
    mat = []
    for i in range(node_index):
        row = []
        for j in range(node_index):
            row.append(0)
        mat.append(row)
    for f, t in edges:
        if f.upper() == f:
            costs[nodes[f]] = len(nodes) * len(nodes)
        else:
            costs[nodes[f]] = 1
        if t.upper() == t:
            costs[nodes[t]] = len(nodes) * len(nodes)
        else:
            costs[nodes[t]] = 1
        if f.upper() == f or t.upper() == t:
            weight = len(nodes) * len(nodes)
        else:
            weight = 1
        mat[nodes[f]][nodes[t]] = weight
        mat[nodes[t]][nodes[f]] = weight
    return nodes, costs, mat, {v: k for k, v in nodes.items()}


def small_cave_once(
    end: int,
    current: int,
    visited: list[int],
    costs: dict[int, int],
    mat: list[list[int]],
    count: int,
) -> int:
    edges = mat[current]
    for i, edge in enumerate(edges):
        if edge > 0:
            if i == end:
                count += 1
                continue
            if i not in visited:
                new_visit = visited
                if costs[i] == 1:
                    new_visit = visited + [i]
                count = small_cave_once(end, i, new_visit, costs, mat, count)
    return count


def small_cave_twice(
    e: int,
    s: int,
    curr: int,
    visited: list[int],
    costs: dict[int, int],
    mat: list[list[int]],
    paths: set[int],
    path: list[int],
    twice: bool,
) -> set[str]:
    edges = mat[curr]
    for i, edge in enumerate(edges):
        if edge > 0:
            if i == e:
                paths.add(str(path))
                continue
            if not twice and costs[i] == 1 and i != e and i != s:
                paths = small_cave_twice(
                    e,
                    s,
                    i,
                    visited,
                    costs,
                    mat,
                    paths,
                    path + [i],
                    True,
                )
            if i not in visited:
                if costs[i] == 1:
                    paths = small_cave_twice(
                        e,
                        s,
                        i,
                        visited + [i],
                        costs,
                        mat,
                        paths,
                        path + [i],
                        twice,
                    )
                else:
                    paths = small_cave_twice(
                        e,
                        s,
                        i,
                        visited,
                        costs,
                        mat,
                        paths,
                        path + [i],
                        twice,
                    )
    return paths


def solution(input_: Input) -> tuple[int, int]:
    nodes, costs, mat, rev_nodes = input_
    s = nodes["start"]
    e = nodes["end"]
    return (
        small_cave_once(e, s, [s], costs, mat, 0),
        len(small_cave_twice(e, s, s, [s], costs, mat, set(), [s], False)),
    )


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        part1, part2 = solution(data)
        print(part1)
        print(part2)
