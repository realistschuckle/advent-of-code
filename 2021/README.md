# Advent of Code 2021

My Advent of Code solutions for 2021.

- [X] [Day 1](./day-01/solution.py) **Sonar Sweep** - fun use of `tee`
- [X] [Day 2](./day-02/solution.py) **Dive!** - `lambda`s in `dict`ionaries.
- [X] [Day 3](./day-03/solution.py) **Binary Diagnostic** - an exercise in `zip`s and binary partitioning
- [X] [Day 4](./day-04/solution.py) **Giant Squid** - an OO solution to the problem
- [X] [Day 5](./day-05/solution.py) **Hydrothermal Ventrue** - nothing to see, here
- [X] [Day 6](./day-06/solution.py) **Lanternfish** - busting exponential growth!
- [X] [Day 7](./day-07/solution.py) **The Treachery of Whales** - cute use of (n(n+1))/2
- [X] [Day 8](./day-08/solution.py) **Seven Segment Search** - just logic
- [X] [Day 9](./day-09/solution.py) **Smoke Basin** - command line opts to see basin map
- [X] [Day 10](./day-10/solution.py) **Syntax Scoring** - a nice use of a stack
- [X] [Day 11](./day-11/solution.py) **Dumbo Octopus** - OMG, so cute, and first use of recursion in a solution
- [X] [Day 12](./day-12/solution.py) **Passage Pathing** - some depth-first-searching
- [X] [Day 13](./day-13/solution.py) **Transparent Origami** - lots of list comprehensions
- [X] [Day 14](./day-14/solution.py) **Extended Polymerization** - another exercise in exponential growth.
- [X] [Day 15](./day-15/solution.py) **Chiton** - well, more exponential growth and shortest-path for which I used Djikstra's to get answers (Part 1 in 15 seconds, Part 2 in 1 hour, 10 minutes), but then refactored it to a hand-crafted algorithm that I designed on paper
- [X] [Day 16](./day-16/solution.py) **Packet Decoder** - that was just tedious, really
- [X] [Day 17](./day-17/solution.py) **Trick Shot** - this is really just some math to figure out bounds to search
- [X] [Day 18](./day-18/solution.py) **Snailfish** - graph traversal
- [ ] Day 19
- [ ] Day 20
- [ ] Day 21
- [ ] Day 22
- [ ] Day 23
- [ ] Day 24
- [ ] Day 25
