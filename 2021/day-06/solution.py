from typing import Callable

Parser = Callable[[int, str], list[int]]
ProblemInput = list[int]


def parse(line_no: int, line: str) -> ProblemInput:
    return [int(x) for x in line.strip().split(",")]


def read_data(data_file: str, fn: Parser) -> ProblemInput:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)][0]


def solution(data: ProblemInput, days: int) -> int:
    counts = {value: 0 for value in data}
    for value in data:
        counts[value] += 1
    for _ in range(days):
        new = {-1: 0, 6: 0}
        for key, value in counts.items():
            new[key - 1] = value
            parents_count = new.get(-1, 0)
        new[8] = parents_count
        new[6] += parents_count
        del new[-1]
        counts = new
    return sum([v for v in counts.values()])


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data, 80))
        print(solution(data, 256))
