from typing import Callable

Parser = Callable[[int, str], list[int]]
ProblemInput = list[int]


def parse(line_no: int, line: str) -> ProblemInput:
    return [int(x) for x in line.strip().split(",")]


def read_data(data_file: str, fn: Parser) -> ProblemInput:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)][0]


def solution(data: ProblemInput, calc: Callable[(int, int, int), int]) -> int:
    data = sorted(data)
    counts = [0] * (data[-1] + 1)
    for x in data:
        counts[x] += 1
    lo = 0
    hi = data[-1] + 1
    last_sum = 0
    current_sum = 0
    while lo < hi:
        last_sum = current_sum
        pivot = (hi + lo) // 2
        left_sum = sum(
            [calc(pivot, i, x) for i, x in enumerate(counts) if i < pivot]
        )
        right_sum = sum(
            [calc(pivot, i, x) for i, x in enumerate(counts) if i > pivot]
        )
        print((pivot, left_sum, right_sum))
        current_sum = left_sum + right_sum
        if left_sum < right_sum:
            lo = pivot + 1
        elif left_sum > right_sum:
            hi = pivot - 1
        else:
            break
    return pivot, last_sum, current_sum


# example.data
# (3, 37)
# (6, 168)
# problem.data
# (338, 355521)
# (494, 100148777)


def part1(pivot, i, count):
    return count * abs(pivot - i)


def part2(pivot, i, count):
    return count * (abs(pivot - i) * (abs(pivot - i) + 1) // 2)


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data, part1))
        print("-" * 40)
        print(solution(data, part2))
