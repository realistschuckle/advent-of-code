from functools import reduce
from typing import Callable

Input = list[str]


def parse(line_no: int, line: int) -> Input:
    return line.strip()


def solution(input: Input) -> int:
    corrupted_scores = {")": 3, "]": 57, "}": 1197, ">": 25137}
    incomplete_scores = {"(": 1, "[": 2, "{": 3, "<": 4}
    corrupted = []
    incomplete = []
    for i in range(len(input)):
        is_corrupt = False
        line = input[i]
        stack = []
        for c in line:
            if c == "(" or c == "{" or c == "[" or c == "<":
                stack.append(c)
            else:
                o = stack.pop()
                if (
                    (c == ")" and o != "(")
                    or (c == "}" and o != "{")
                    or (c == "]" and o != "[")
                    or (c == ">" and o != "<")
                ):
                    corrupted.append(c)
                    is_corrupt = True
                    break
        if not is_corrupt and len(stack):
            stack_scores = reversed([incomplete_scores[x] for x in stack])
            s = reduce(lambda acc, x: acc * 5 + x, stack_scores, 0)
            incomplete.append(s)
    incomplete = sorted(incomplete)
    corrupted_values = [corrupted_scores[c] for c in corrupted]
    return sum(corrupted_values), incomplete[len(incomplete) // 2]


def read_data(data_file: str, fn: Callable[[int, int], Input]) -> Input:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        corrupted_score, incomplete_score = solution(data)
        print(corrupted_score)
        print(incomplete_score)
