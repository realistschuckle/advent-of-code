from itertools import tee
from typing import Callable

Line = list[int]
Input = list[Line]


def parse(line_no: int, line: str) -> Line:
    if "->" in line:
        pair, insert = line.strip().split(" -> ")
        return pair, insert
    return line.strip() if len(line.strip()) else None


def read_data(data_file: str, fn: Callable[[int, str], Input]) -> Input:
    with open(data_file) as f:
        lines = [
            l
            for l in [fn(i, line) for i, line in enumerate(f)]
            if l is not None
        ]
    return lines[0], lines[1:]


def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def solution(data: Input, n: int) -> int:
    template, insertions = data
    insertions = {k: k[0] + v + k[1] for k, v in insertions}
    pairs = {"".join(pair): 1 for pair in pairwise(template)}
    for i in range(n):
        new_pairs = {}
        for pair, count in pairs.items():
            if pair not in insertions:
                new_pairs[pair] += count
                continue
            insertion = insertions[pair]
            left = insertion[:2]
            right = insertion[1:]
            if left not in new_pairs:
                new_pairs[left] = 0
            if right not in new_pairs:
                new_pairs[right] = 0
            new_pairs[left] += count
            new_pairs[right] += count
        pairs = new_pairs
    counts = {}
    for k, v in pairs.items():
        if k[0] not in counts:
            counts[k[0]] = 0
        counts[k[0]] += v
        if k[1] not in counts:
            counts[k[1]] = 0
        counts[k[1]] += v
    max_value = max(counts.values())
    min_value = min(counts.values())
    delta = 0
    if len(pairs) % 2 == 1:
        delta = 1
    return (max_value - min_value) // 2 + delta


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data, 10))
        print(solution(data, 40))
