from itertools import groupby
from typing import Callable

Parser = Callable[[int, str], list[int]]
ProblemInput = tuple[list[str], list[str]]


def parse(line_no: int, line: str) -> ProblemInput:
    patterns, output = line.strip().split(" | ")
    patterns = groupby(
        [p.strip() for p in sorted(patterns.split(" "), key=len)], key=len
    )
    output = [p.strip() for p in output.split(" ")]
    return patterns, output


def read_data(data_file: str, fn: Parser) -> ProblemInput:
    with open(data_file) as f:
        return [fn(i, line) for i, line in enumerate(f)]


def count1478(data: ProblemInput) -> int:
    count = 0
    for _, output in data:
        for value in output:
            lv = len(value)
            if lv == 2 or lv == 3 or lv == 4 or lv == 7:
                count += 1
    return count


def decode(data: ProblemInput) -> int:
    sum = 0
    for entries, outputs in data:
        key = []
        for _ in range(7):
            key.append([])
        for len_, patterns in entries:
            patterns = list(patterns)
            if len_ == 2:
                for c in patterns[0]:
                    key[2].append(c)
                    key[5].append(c)
            elif len_ == 3:
                for c in patterns[0]:
                    if c not in key[2] and c not in key[5]:
                        key[0] = c
            elif len_ == 4:
                for c in patterns[0]:
                    if c not in key[2] and c not in key[5]:
                        key[1].append(c)
                        key[3].append(c)
            elif len_ == 5:
                three = None
                for pattern in patterns:
                    # find 3
                    pattern = pattern.replace(key[0], "")
                    for c in key[2]:
                        pattern = pattern.replace(c, "")
                    for c in key[3]:
                        pattern = pattern.replace(c, "")
                    if len(pattern) == 1:
                        key[6] = pattern[0]
                        three = pattern
                        break
                for pattern in patterns:
                    if pattern == three:
                        continue
                    # find 2
                    pattern = pattern.replace(key[0], "")
                    pattern = pattern.replace(key[6], "")
                    for c in key[2]:
                        pattern = pattern.replace(c, "")
                    for c in key[3]:
                        pattern = pattern.replace(c, "")
                    if len(pattern) == 1:
                        key[4] = pattern[0]
                        break
            elif len_ == 6:
                zero = None
                for pattern in patterns:
                    # find 0
                    pattern = pattern.replace(key[0], "")
                    pattern = pattern.replace(key[4], "")
                    pattern = pattern.replace(key[6], "")
                    for c in key[2]:
                        pattern = pattern.replace(c, "")
                    if len(pattern) == 1:
                        key[1] = pattern[0]
                        key[3] = [x for x in key[3] if x != key[1]][0]
                        zero = pattern
                        break
                for pattern in patterns:
                    if pattern == zero:
                        continue
                    pattern = pattern.replace(key[0], "")
                    pattern = pattern.replace(key[1], "")
                    pattern = pattern.replace(key[3], "")
                    pattern = pattern.replace(key[4], "")
                    pattern = pattern.replace(key[6], "")
                    if len(pattern) == 1:
                        key[5] = pattern[0]
                        key[2] = [x for x in key[2] if x != key[5]][0]
                        break
        codes = [
            "".join(sorted([key[x] for x in [0, 1, 2, 4, 5, 6]])),
            "".join(sorted([key[x] for x in [2, 5]])),
            "".join(sorted([key[x] for x in [0, 2, 3, 4, 6]])),
            "".join(sorted([key[x] for x in [0, 2, 3, 5, 6]])),
            "".join(sorted([key[x] for x in [1, 2, 3, 5]])),
            "".join(sorted([key[x] for x in [0, 1, 3, 5, 6]])),
            "".join(sorted([key[x] for x in [0, 1, 3, 4, 5, 6]])),
            "".join(sorted([key[x] for x in [0, 2, 5]])),
            "abcdefg",
            "".join(sorted([key[x] for x in [0, 1, 2, 3, 5, 6]])),
        ]
        value = 0
        for output in outputs:
            for i, code in enumerate(codes):
                if sorted(code) == sorted(output):
                    value = value * 10 + i
                    break
        sum += value
    return sum


if __name__ == "__main__":
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(count1478(data))
        print(decode(data))
