from math import ceil
import re
from typing import Callable

Line = list[int]
Input = list[Line]
LineParser = Callable[[int, str], Line]
Parser = Callable[[int, str], Input]


def parseList(*converters, sep=",", test=None) -> LineParser:
    def parser(line_no: int, line: str, show_debug=False) -> Line:
        pieces = line.strip()
        if sep is not None:
            pieces = re.compile(sep).split(pieces)
        if show_debug:
            print("pieces", pieces)
        return [
            converters[i](x)
            for i, x in enumerate(pieces)
            if converters[i] is not None
            and (test is None or test(line_no, line))
        ]

    return parser


def parse(line_no: int, line: str, show_debug=False) -> Line:
    return [int(x) for x in line if len(x.strip())]


def read_data(data_file: str, fn: Parser, show_debug=False) -> Input:
    with open(data_file) as f:
        return [fn(i, line, show_debug) for i, line in enumerate(f)]


def sum_naturals(n):
    return (n * (n + 1)) // 2


def y_pos_factory(vel_y):
    def y_pos(i):
        return i * vel_y - sum_naturals(i - 1)

    return y_pos


def x_pos_factory(vel_x):
    vel_x_max = sum_naturals(vel_x)

    def x_pos(i):
        diff = sum_naturals(vel_x - i) if i <= vel_x else 0
        value = vel_x_max - diff
        return min(vel_x_max, value)

    return x_pos


def solution(data: Input, show_debug) -> int:
    target_min_x, target_max_x, target_min_y, target_max_y = data[0]
    if show_debug:
        print("input", target_min_x, target_max_x, target_min_y, target_max_y)
    # The minimum value of the x velocity such that it peters
    # out within the target range, same with y
    vel_min_x = 0
    vel_max_x = target_max_x
    vel_min_y = target_min_y
    vel_max_y = -target_min_y
    for velocity in range(target_min_x):
        final_pos = (velocity) * (velocity + 1) // 2
        if final_pos >= target_min_x:
            if vel_min_x == 0:
                vel_min_x = velocity
            else:
                break
    if show_debug:
        print("x velocity range to 0:", vel_min_x, vel_max_x)
        print("y velocity range from 0:", vel_min_y, vel_max_y)

    # PART I
    # Need only check the y velocity ranges that do not
    # overshoot the target range after coming back down
    # from the apex, assuming that we need only check
    # positive values
    best_y = 0
    for vel_y in range(1, abs(target_min_y)):
        best_y_for_vel = 0
        y_pos = y_pos_factory(vel_y)
        if show_debug == 2:
            print(vel_y, end=": ")
        i = vel_min_x
        while True:
            pos_y = y_pos(i)
            best_y_for_vel = max(best_y_for_vel, pos_y)
            if show_debug == 2:
                print(pos_y, end=" ")
            if pos_y < target_min_y:
                break
            i += 1
        if show_debug == 2:
            print()
        best_y = max(best_y, best_y_for_vel)

    # PART II
    # Initialize the count with all of the values that directly put the
    # probe into the target area
    width = abs(target_max_x - target_min_x) + 1
    height = abs(target_min_y - target_max_y) + 1
    vel_count = width * height
    highest_indirect_vel_x = ceil(target_max_x / 2)
    if show_debug:
        print("Number of direct velocities:", vel_count)
        print(
            "Investigate indirect x velocities from",
            vel_min_x,
            "to",
            highest_indirect_vel_x,
        )
    for vel_x in range(vel_min_x, highest_indirect_vel_x + 1):
        x_pos = x_pos_factory(vel_x)
        for vel_y in range(vel_min_y, vel_max_y + 1):
            y_pos = y_pos_factory(vel_y)
            i = 1
            if show_debug == 2:
                print("Investigating x vel, y_vel", vel_x, ",", vel_y)
            while True:
                y = y_pos(i)
                x = x_pos(i)
                if (
                    x >= target_min_x
                    and x <= target_max_x
                    and y >= target_min_y
                    and y <= target_max_y
                ):
                    if show_debug == 2:
                        print("  Found velocity at", i, x, y)
                    elif show_debug:
                        print(
                            "Found solution for velocity ", vel_x, ",", vel_y
                        )
                    vel_count += 1
                    break
                elif x > target_max_x or y < target_min_y:
                    if show_debug == 2:
                        print("  Rejected velocity at", i, x, y)
                    break
                else:
                    if show_debug == 2:
                        print("  Ignored velocity at", i, x, y)
                i += 1
    return best_y, vel_count


if __name__ == "__main__":
    import sys

    show_debug = 0
    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
                                       [--debug[=level]]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message.
    --debug[=1], -d  Print debug information
    --debug=2, -dd   Print more debug information"""
            )
            exit(0)
        elif arg == "--debug" or arg == "--debug=1" or arg == "-d":
            show_debug = 1
        elif arg == "--debug=2" or arg == "-dd":
            show_debug = 2
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        if show_debug:
            print("=" * 40)
        parser = parseList(None, None, int, int, int, int, sep="[ xy=\\.,]+")
        data = read_data(data_file, parser, show_debug)
        best_y, total_velocities = solution(data, show_debug)
        print(best_y)
        print(total_velocities)
        if show_debug:
            print("\n")
            print("=" * 40)
