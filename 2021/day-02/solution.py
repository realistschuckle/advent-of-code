from collections.abc import Callable
from typing import Dict


def parse(line):
    direction, amount = line.split(" ")
    return direction, int(amount)


def read_data(data_file, fn):
    with open(data_file) as f:
        return [fn(line) for line in f]


def solution(
    movements: list[tuple[str, int]],
    commands: Dict[str, Callable[int, Dict[str, int]]],
) -> int:
    state = {"aim": 0, "depth": 0, "position": 0}
    for direction, amount in movements:
        instructions = commands[direction]
        if callable(instructions):
            instructions = [instructions]
        for instruction in instructions:
            state.update(instruction(amount, state))
    return state["depth"] * state["position"]


if __name__ == "__main__":
    simple = {
        "up": lambda amount, state: {"depth": state["depth"] - amount},
        "down": lambda amount, state: {"depth": state["depth"] + amount},
        "forward": lambda amount, state: {
            "position": state["position"] + amount
        },
    }

    modified = {
        "up": lambda amount, state: {"aim": state["aim"] - amount},
        "down": lambda amount, state: {"aim": state["aim"] + amount},
        "forward": [
            lambda amount, state: {"position": state["position"] + amount},
            lambda amount, state: {
                "depth": state["depth"] + state["aim"] * amount
            },
        ],
    }
    for data_file in ["example.data", "problem.data"]:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data, simple))
        print(solution(data, modified))
