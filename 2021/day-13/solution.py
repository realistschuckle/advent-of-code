from typing import Callable, Union

Line = tuple[Union[None, str, int], int]
Input = tuple[list[tuple[int, int]], list[tuple[str, int]]]


def parse(line_no: int, line: str) -> Line:
    if len(line.strip()) == 0:
        return (None, 0)
    if line[0] == "f":
        from_, to_ = line.strip().split("=")
        return from_[-1], int(to_)
    x, y = line.strip().split(",")
    return int(x), int(y)


def read_data(data_file: str, fn: Callable[[int, str], Input]) -> Input:
    with open(data_file) as f:
        instructions = [fn(i, line) for i, line in enumerate(f)]
    dots = [
        (x, y)
        for x, y in instructions
        if x is not None and not hasattr(x, "split")
    ]
    folds = [
        (x, y)
        for x, y in instructions
        if x is not None and hasattr(x, "split")
    ]
    return dots, folds


def solution(input_: Input) -> tuple[int, tuple[int, int], int, int]:
    dots, folds = input_
    max_x = max([x for x, _ in dots])
    max_y = max([y for _, y in dots])
    dims = [max_x, max_y]
    first_fold_count = -1
    funcs = [
        lambda x, y, axis: (2 * axis - x, y),
        lambda x, y, axis: (x, 2 * axis - y),
    ]
    for dir_, axis in folds:
        index = 0 if dir_ == "x" else 1
        dots = set(
            [d for d in dots if d[index] < axis]
            + [funcs[index](*d, axis) for d in dots if d[index] > axis]
        )
        dims[index] = axis
        if first_fold_count == -1:
            first_fold_count = len(dots)
    return first_fold_count, dots, *dims


def print_dots(dots, bottom, right):
    for y in range(bottom):
        for x in range(right):
            value = " "
            if (x, y) in dots:
                value = "█"
            print(value, end="")
        print()


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        first_fold_count, dots, right, bottom = solution(data)
        print(first_fold_count)
        print_dots(dots, bottom, right)
