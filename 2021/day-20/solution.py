import re
from typing import Callable

Line = list[str]
Input = list[Line]
Parser = Callable[[int, str], Line]


def parseList(*converters, sep=",", test=None) -> Parser:
    def parser(line_no: int, line: str) -> Line:
        pieces = line
        if sep is not None:
            pieces = re.compile(sep).split(line)
        return [
            converters[i](line_no, x)
            if len(converters) > 1
            else converters[0](line_no, x)
            for i, x in enumerate(pieces)
            if (len(converters) == 1 or converters[i] is not None)
            and (test is None or test(line_no, line))
        ]

    return parser


def parse(line_no: int, line: str) -> Line:
    return [int(x) for x in line if len(x.strip())]


def read_data(data_file: str, fn: Callable[[int, str], Input]) -> Input:
    with open(data_file) as f:
        return [fn(i, line.strip()) for i, line in enumerate(f)]


def new_image(height, width, fill):
    image = []
    for _ in range(height):
        image.append([fill] * width)
    return image


def expand_image(image, height, width, margin, fill):
    expanded = []
    for _ in range(margin):
        expanded.append([fill] * (width + 2 * margin))
    for y in range(len(image)):
        left_padding = [fill] * margin
        right_padding = [fill] * margin
        expanded.append([*left_padding, *image[y], *right_padding])
    for _ in range(margin):
        expanded.append([fill] * (width + 2 * margin))
    return expanded


def compress(image, fill):
    i = 0
    while True:
        if all([c == fill for c in image[i]]):
            image = image[1:]
            continue
        break
    while True:
        if all([c == fill for c in image[-1]]):
            image = image[0:-1]
            continue
        break
    while True:
        transpose = zip(*image)
        first_column = transpose.__next__()
        if all([c == fill for c in first_column]):
            for i, line in enumerate(image):
                image[i] = line[1:]
            continue
        break
    while True:
        r = list(reversed([reversed(line) for line in image]))
        transpose = zip(*r)
        last_column = transpose.__next__()
        if all([c == fill for c in last_column]):
            for i, line in enumerate(image):
                image[i] = line[:-1]
            continue
        break
    return image


def solution(data: Input, applications: int) -> int:
    algorithm, _, *image = data
    margin = 2
    for n in range(applications):
        height = len(image)
        width = len(image[0])
        fill = "0" if algorithm[0] == "0" else algorithm[(n % 2) * 511]
        expanded = expand_image(image, height, width, margin, fill)
        next_image = new_image(len(expanded), len(expanded[0]), fill)
        for y in range(1, len(expanded) - 1):
            for x in range(1, len(expanded[0]) - 1):
                buffer = [
                    *expanded[y - 1][x - 1 : x + 2],
                    *expanded[y][x - 1 : x + 2],
                    *expanded[y + 1][x - 1 : x + 2],
                ]
                index = int("".join(buffer), base=2)
                next_image[y][x] = algorithm[index]
        image = compress(next_image, fill)
    for line in image:
        print("".join(["." if c == "0" else "#" for c in line]))
    return sum([sum([int(c) for c in line]) for line in image])


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    parser = parseList(
        lambda i, x: "1" if x == "#" else "0",
        sep=None,
        test=lambda i, x: len(x) > 0,
    )
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parser)
        print(solution(data, 2))
