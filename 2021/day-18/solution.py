from itertools import permutations
from math import floor, ceil
from typing import Callable

Line = list[int]
Input = list[Line]
Parser = Callable[[int, str], Line]


class NoneNode:
    @property
    def is_none(self):
        return True

    def _indent_str(self, indent):
        return " " * indent + "None\n"


class Node:
    INDENT_STRING = "  "

    def __init__(self, parent, left=None, right=None):
        self.parent = parent
        self.left = left if left is not None else NoneNode()
        self.right = right if right is not None else NoneNode()

    @property
    def depth(self):
        if self.parent is None:
            return 0
        return self.parent.depth + 1

    @property
    def is_none(self):
        return False

    @property
    def magnitude(self):
        left = self.left if type(self.left) is int else self.left.magnitude
        right = self.right if type(self.right) is int else self.right.magnitude
        return 3 * left + 2 * right

    def clone(self, parent=None):
        new_node = Node(parent)
        left = self.left
        if type(left) is not int:
            left = self.left.clone(new_node)
        new_node.left = left
        right = self.right
        if type(right) is not int:
            right = self.right.clone(new_node)
        new_node.right = right
        return new_node

    def set_child(self, node):
        if not hasattr(self.left, "is_none") or self.left.is_none:
            self.left = node
        else:
            self.right = node

    def _explode(self):
        if self.depth < 4:
            changed = False
            if hasattr(self.left, "_explode"):
                changed = self.left._explode()
            if not changed and hasattr(self.right, "_explode"):
                changed = self.right._explode()
            return changed

        # LEFT EXPLOSION
        # Find parent that has integer left value and add to it
        target = self.parent
        left = self
        while target is not None and target.left == left:
            left = target
            target = target.parent
        if target is not None:
            if type(target.left) is int:
                target.left += self.left
            else:
                target = target.left
                while type(target.right) is not int:
                    target = target.right
                target.right += self.left

        # RIGHT EXPLOSION
        # Find parent that has an integer right value and add to it
        target = self.parent
        right = self
        while target is not None and target.right == right:
            right = target
            target = target.parent
        if target is not None:
            if type(target.right) is int:
                target.right += self.right
            else:
                target = target.right
                while type(target.left) is not int:
                    target = target.left
                target.left += self.right

        # ZERO OUT
        # Zero out the parent reference to self
        if self.parent.left == self:
            self.parent.left = 0
        else:
            self.parent.right = 0

        return True

    def _split(self):
        if type(self.left) is int and self.left > 9:
            split_value = self.left / 2
            self.left = Node(self, floor(split_value), ceil(split_value))
            return True
        changed = type(self.left) is not int and self.left._split()
        if changed:
            return True
        if type(self.right) is int and self.right > 9:
            split_value = self.right / 2
            self.right = Node(self, floor(split_value), ceil(split_value))
            return True
        return type(self.right) is not int and self.right._split()

    def _indent_str(self, indent):
        ind = self.INDENT_STRING * indent
        if type(self.left) is int and type(self.right) is int:
            return ind + f"{self.depth} {self.left} {self.right}\n"
        elif type(self.left) is int:
            return (
                ind
                + f"{self.depth} {self.left} X\n"
                + self.right._indent_str(indent + 1)
            )
        elif type(self.right) is int:
            return (
                ind
                + f"{self.depth} X {self.right}\n"
                + self.left._indent_str(indent + 1)
            )
        else:
            return (
                ind
                + f"{self.depth} X X\n"
                + self.left._indent_str(indent + 1)
                + self.right._indent_str(indent + 1)
            )

    def __add__(self, node):
        parent = Node(None, self, node)
        parent.left.parent = parent
        parent.right.parent = parent
        changed = True
        while changed:
            changed = parent._explode()
            if changed:
                continue
            changed = parent._split()
        return parent

    def __str__(self):
        return self._indent_str(0)

    def __repr__(self):
        return self.__str__()


def parse_visit(parent, lst):
    x, y = lst
    if type(x) is int:
        parent.left = x
    else:
        parent.left = Node(parent)
        parse_visit(parent.left, x)
    if type(y) is int:
        parent.right = y
    else:
        parent.right = Node(parent)
        parse_visit(parent.right, y)


def parse(line_no: int, line: str) -> Line:
    lst = eval(line)
    root = Node(None)
    parse_visit(root, lst)
    return root


def read_data(data_file: str, fn: Callable[[int, str], Input]) -> Input:
    with open(data_file) as f:
        return [
            fn(i, line) for i, line in enumerate(f) if not line.startswith("#")
        ]


def solution(data: Input) -> int:
    sum_ = data[0].clone()
    for n in data[1:]:
        sum_ += n.clone()
    max_pair_mag = -1
    for pair in permutations(enumerate(data), 2):
        (li, l), (ri, r) = pair
        s = l.clone() + r.clone()
        pair_mag = s.magnitude
        if pair_mag > max_pair_mag:
            max_pair_mag = pair_mag
    return sum_.magnitude, max_pair_mag


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file, parse)
        print(solution(data))
