from pprint import pprint
import re
from typing import Callable

Line = list[int]
Input = list[Line]
Parser = Callable[[int, str], Line]


def parseList(*converters, sep=",", test=None) -> Parser:
    def parser(line_no: int, line: str) -> Line:
        pieces = line
        if sep is not None:
            pieces = re.compile(sep).split(line)
        return [
            converters[i](x)
            for i, x in enumerate(pieces)
            if converters[i] is not None
            and (test is None or test(line_no, line))
        ]

    return parser


def read_data(data_file: str) -> Input:
    with open(data_file) as f:
        lines = [line.strip() for line in f.readlines()]
    parser = parseList(int, int, int)
    scanner_readings = []
    current_reading = []
    for line in lines:
        if line.startswith("---"):
            continue
        elif len(line) == 0:
            scanner_readings.append(current_reading)
            current_reading = []
        else:
            value = parser(None, line)
            current_reading.append(value)
    scanner_readings.append(current_reading)
    return scanner_readings


def solution(data: Input) -> int:
    pprint(data)


if __name__ == "__main__":
    import sys

    show_map = False
    files = []
    for arg in sys.argv[1:]:
        if not arg[0] == "-":
            files.append(arg)
        if arg == "--help" or arg == "-h":
            print(
                """python3 solution.py [input-file-name...] [--help]
    input-file-name  The name of the input file to use in the execution.
    --help, -h       Print this message."""
            )
            exit(0)
    if not files:
        files = ["example.data", "problem.data"]
    for data_file in files:
        print(data_file)
        data = read_data(data_file)
        print(solution(data))
