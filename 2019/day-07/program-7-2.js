const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

class Amplifier {
  constructor(phase, program) {
    this.program = program;
    this.phase = phase;
    this.ip = 0;
    this.halted = false;
    this.notRun = true;
  }

  getInstructionAt(index) {
    const program = this.program;
    const inst = program[index];
    const opcode = inst % 100;
    const n = opcode === 1 || opcode === 2 || opcode === 7 || opcode === 8 ? 3
      : opcode === 3 || opcode === 4 ? 1
        : opcode === 5 || opcode === 6 ? 2
          : 0;
    const modes = inst.toString()
      .substring(0, inst.toString().length - 2)
      .split('')
      .map(x => Number.parseInt(x))
      .reverse();
    while (modes.length < n) {
      modes.push(0);
    }

    const params = [];

    for (let i = 0; i < n; i += 1) {
      if ([1, 2, 3, 7, 8].includes(opcode) && i === n - 1) {
        params.push(program[index + i + 1]);
      } else if (modes[i] === 0) {
        params.push(program[program[index + i + 1]]);
      } else if (modes[i] === 1) {
        params.push(program[index + i + 1]);
      }
    }

    return {
      opcode,
      params,
      n,
    };
  }

  run(input) {
    const program = this.program;
    let waitingForSignal = false;
    let inputInstruction = this.notRun ? 1 : 2;
    for (let i = this.ip; i < program.length && !this.halted && !waitingForSignal; i += 1) {
      const { opcode, params, n } = this.getInstructionAt(i);
      switch (opcode) {
        case 1: {
          let left = params[0];
          let right = params[1];
          let position = params[2];
          program[position] = left + right;
          i += n;
          break;
        }
        case 2: {
          let left = params[0];
          let right = params[1];
          let position = params[2];
          program[position] = left * right;
          i += n;
          break;
        }
        case 3: {
          let position = params[0];
          if (inputInstruction === 1) {
            program[position] = this.phase;
          } else if (inputInstruction === 2) {
            program[position] = input;
          } else {
            this.ip = i;
            waitingForSignal = true;
          }
          inputInstruction += 1;
          i += n;
          break;
        }
        case 4: {
          let value = params[0];
          this.output = value;
          i += n;
          break;
        }
        case 5: {
          if (params[0] !== 0) {
            i = params[1] - 1;
          } else {
            i += n;
          }
          break;
        }
        case 6: {
          if (params[0] === 0) {
            i = params[1] - 1;
          } else {
            i += n;
          }
          break;
        }
        case 7: {
          if (params[0] < params[1]) {
            program[params[2]] = 1;
          } else {
            program[params[2]] = 0;
          }
          i += n;
          break;
        }
        case 8: {
          if (params[0] === params[1]) {
            program[params[2]] = 1;
          } else {
            program[params[2]] = 0;
          }
          i += n;
          break;
        }
        case 99: {
          this.halted = true;
          break;
        }
        default: {
          throw "FAILURE AT " + i + " WITH OPCODE " + opcode;
        }
      }
    }
    this.notRun = false;
  }
}

(async function () {
  const content = await read(pathJoin(__dirname, '/data-7.txt'), 'utf-8');
  const makeProgram = () => content.trim().split(',').map(i => Number.parseInt(i));

  function permute(source) {
    const output = [];
    for (let i = 0; i < source.length; i = i + 1) {
      let rest = permute(source.slice(0, i).concat(source.slice(i + 1)));

      if (!rest.length) {
        output.push([source[i]])
      } else {
        for (let j = 0; j < rest.length; j = j + 1) {
          output.push([source[i]].concat(rest[j]))
        }
      }
    }
    return output;
  }
  const testPhases = permute([5, 6, 7, 8, 9]);

  const outputs = [];
  for (let testPhase of testPhases) {
    let input = 0;
    const amplifiers = [
      new Amplifier(testPhase[0], makeProgram()),
      new Amplifier(testPhase[1], makeProgram()),
      new Amplifier(testPhase[2], makeProgram()),
      new Amplifier(testPhase[3], makeProgram()),
      new Amplifier(testPhase[4], makeProgram()),
    ];

    while (true) {
      const amplifier = amplifiers.shift();
      if (amplifier.halted) {
        break;
      }
      amplifier.run(input);
      input = amplifier.output;
      amplifiers.push(amplifier);
    }

    outputs.push(input);
  }
  return Math.max.apply(null, outputs);;
}())
  .then(console.log)
  .catch(console.error);
