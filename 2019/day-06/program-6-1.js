const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, '/data-6.txt'), 'utf-8');
  const program = content
    .trim()
    .split('\n')
    .map(orbit => orbit.split(")"));

  const orbits = new Map();
  for (let [center, orbiter] of program) {
    orbits.set(orbiter, center);
  }

  let total = 0;

  function getToCom(value) {
    let path = 0;
    while (value !== "COM") {
      value = orbits.get(value);
      if (value === undefined) {
        process.exit();
      }
      path += 1;
    }
    return path;
  }

  for (let key of orbits.keys()) {
    total += getToCom(key);
  }

  return total;

}())
  .then(console.log)
  .catch(console.error);
