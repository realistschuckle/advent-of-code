const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, 'data-8.txt'), 'utf-8');
  const program = content.trim();

  const pieces = [];
  for (let i = 0; i < program.length; i += 25 * 6) {
    pieces.push(program.substr(i, 25 * 6));
  }

  const layers = pieces
    .reduce((a, c) => {
      const num0s = c.replace(/[^0]/g, '').length;
      a[num0s] = c;
      return a;
    }, []);
  let smallest = [];
  for (let i in layers) {
    if (layers[i] !== undefined) {
      smallest = layers[i];
      break;
    }
  }

  return smallest.replace(/[^1]/g, '').length * smallest.replace(/[^2]/g, '').length;
}())
  .then(console.log)
  .catch(console.error);
