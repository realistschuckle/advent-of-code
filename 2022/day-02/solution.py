class Shape:
    def __eq__(self, other):
        return type(other) == type(self)


class Paper(Shape):
    def __gt__(self, other):
        return type(other) == Rock

    @property
    def score(self):
        return 2


class Scissors(Shape):
    def __gt__(self, other):
        return type(other) == Paper

    @property
    def score(self):
        return 3


class Rock(Shape):
    def __gt__(self, other):
        return type(other) == Scissors

    @property
    def score(self):
        return 1


def letter2shape(letter: str) -> Shape:
    if letter == "A" or letter == "X":
        return Rock()
    elif letter == "B" or letter == "Y":
        return Paper()
    else:
        return Scissors()


class Round:
    def __init__(self, them: str, me: str):
        self.them = letter2shape(them)
        self.me = letter2shape(me)

        deltas = {
            "X": [0, 2],
            "Y": [3, 1],
            "Z": [6, 0],
        }
        win, offset = deltas[me]
        self.outcome = win + ((self.them.score - offset) % 3) + 1

    def first_score(self):
        outcome = (
            6 if self.me > self.them else 3 if self.me == self.them else 0
        )
        return self.me.score + outcome

    def second_score(self):
        return self.outcome


def solution1(rounds: list[Round]) -> int:
    return sum([r.first_score() for r in rounds])


def solution2(rounds: list[Round]) -> int:
    return sum([r.second_score() for r in rounds])


if __name__ == "__main__":
    with open("input.txt") as f:
        rounds = [Round(*line.strip().split(" ")) for line in f.readlines()]
        outcome = solution1(rounds)
        print(outcome)
        outcome = solution2(rounds)
        print(outcome)
