def solution1(directories: dict[str, list[int]]) -> int:
    total = 0
    totals = {}
    for path in directories:
        path_total = sum(directories[path])
        for other in directories:
            if path != other and other.startswith(path):
                path_total += sum(directories[other])
        if path_total <= 100_000:
            total += path_total
        totals[path] = path_total
    return total, totals


def solution2(totals: dict[str, int])-> int:
    MAX_SIZE = 70000000
    NEEDED_SIZE = 30000000
    available_space = MAX_SIZE - totals["/"]
    needed_removed = NEEDED_SIZE - available_space
    return list(sorted([v for v in totals.values() if v >= needed_removed]))[0]


if __name__ == "__main__":
    directories = {}
    path = ""
    with open("input.txt") as f:
        for line in f.readlines():
            line = line.strip()
            if line == "$ cd ..":
                path = path[: path.rfind("/", 0, -1) + 1]
            elif line == "$ cd /":
                path = "/"
            elif line.startswith("$ cd"):
                path += line[4:].strip() + "/"
            elif line.startswith("$ ls"):
                directories[path] = []
            elif line.startswith("dir"):
                continue
            else:
                directories[path].append(int(line.split(" ")[0]))

    result, totals = solution1(directories)
    print(result)
    print(solution2(totals))
