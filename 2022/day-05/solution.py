from copy import deepcopy


class Inst:
    def __init__(self, amt, src, dest):
        self.amt = int(amt)
        self.src = int(src)
        self.dest = int(dest)

    def apply1(self, stacks: list[list[str]]):
        src = stacks[self.src - 1]
        dest = stacks[self.dest - 1]
        for _ in range(self.amt):
            dest.append(src.pop())

    def apply2(self, stacks: list[list[str]]):
        src = stacks[self.src - 1]
        dest = stacks[self.dest - 1]
        tmp = []
        for _ in range(self.amt):
            tmp.append(src.pop())
        for _ in range(self.amt):
            dest.append(tmp.pop())

    def __str__(self):
        return f"{self.amt} {self.src}->{self.dest}"


def solution1(stacks: list[list[str]], insts: list[Inst]) -> str:
    for inst in insts:
        inst.apply1(stacks)
    return "".join([stack[-1] for stack in stacks])


def solution2(stacks: list[list[str]], insts: list[Inst]) -> str:
    for inst in insts:
        inst.apply2(stacks)
    return "".join([stack[-1] for stack in stacks])


def pluck(line, *args):
    result = []
    for arg in args:
        result.append(line[arg])
    return result


if __name__ == "__main__":
    with open("input.txt") as f:
        stacks = [
            list(reversed(a))
            for a in zip(
                *[
                    pluck(line, 1, 5, 9, 13, 17, 21, 25, 29, 33)
                    for line in f.readlines()
                    if line.strip()
                    and not line.strip().startswith("move")
                    and not line.strip().startswith("1")
                ]
            )
        ]
        for stack in stacks:
            while len(stack[-1].strip()) == 0:
                stack.pop()
        f.seek(0)
        instructions = [
            Inst(amt, src, dest)
            for _, amt, __, src, ___, dest in [
                line.split(" ")
                for line in f.readlines()
                if line.startswith("move")
            ]
        ]
        result = solution1(deepcopy(stacks), instructions)
        print(result)
        result = solution2(deepcopy(stacks), instructions)
        print(result)
