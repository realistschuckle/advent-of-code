def solution1(assignments):
    count = 0
    for a, b, c, d in assignments:
        if a <= c <= b and a <= d <= b:
            count += 1
        elif c <= a <= d and c <= b <= d:
            count += 1
    return count


def solution2(assignments):
    count = 0
    for a, b, c, d in assignments:
        if a <= c <= b or a <= d <= b:
            count += 1
        elif c <= a <= d or c <= b <= d:
            count += 1
    return count


if __name__ == "__main__":
    with open("input.txt") as f:
        nums = [
            int(num)
            for line in f.readlines()
            for vals in line.strip().split(",")
            for num in vals.split("-")
        ]
        assignments = [nums[i : i + 4] for i in range(0, len(nums), 4)]
        num_slackers = solution1(assignments)
        print(num_slackers)
        num_slackers = solution2(assignments)
        print(num_slackers)
