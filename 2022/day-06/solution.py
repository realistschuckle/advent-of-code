def solution1(stream: str) -> int:
    for i in range(len(stream) - 4):
        if len(set(stream[i : i + 4])) == 4:
            return i + 4


def solution2(stream: str) -> int:
    for i in range(len(stream) - 14):
        if len(set(stream[i : i + 14])) == 14:
            return i + 14


if __name__ == "__main__":
    with open("input.txt", encoding="utf-8") as f:
        stream = f.read()

        result = solution1(stream)
        print(result)
        result = solution2(stream)
        print(result)
