from copy import deepcopy


def row(values: list[list[int]], row: int) -> list[int]:
    return values[row]


def col(values: list[list[int]], col: int) -> list[int]:
    return [row[col] for row in values]


def visible(values: list[list[int]], row_index: int, col_index: int):
    value = values[row_index][col_index]
    value_row = row(values, row_index)
    value_col = col(values, col_index)

    return (
        max(value_row[0:col_index]) < value
        or max(value_row[col_index+1:]) < value
        or max(value_col[0:row_index]) < value
        or max(value_col[row_index+1:]) < value
    )


def solution1(values: list[list[int]]) -> int:
    total = 0
    for i in range(1, len(values) - 1):
        for j in range(1, len(values[0]) - 1):
            if visible(values, i, j):
                total += 1
    return total + 2 * len(values) + 2 * len(values[0]) - 4


def take(values: list[int], mark: int):
    result = []
    for value in values:
        result.append(value)
        if value >= mark:
            break
    return result


def scene(values: list[list[int]], row_index: int, col_index: int) -> int:
    value = values[row_index][col_index]
    value_row = row(values, row_index)
    value_col = col(values, col_index)

    left = take(reversed(value_row[0:col_index]), value)
    right = take(value_row[col_index+1:], value)
    up = take(reversed(value_col[0:row_index]), value)
    down = take(value_col[row_index+1:], value)
    return len(left) * len(right) * len(up) * len(down)


def solution2(values: list[list[int]]) -> int:
    max_scenic = 0
    for row_index in range(1, len(values) - 1):
        for col_index in range(1, len(values[row_index]) - 1):
            scenic_score = scene(values, row_index, col_index)
            max_scenic = max(max_scenic, scenic_score)
    return max_scenic


if __name__ == "__main__":
    with open("input.txt") as f:
        lines = [[int(i) for i in list(l.strip())] for l in f.readlines()]
        print(solution1(deepcopy(lines)))
        print(solution2(deepcopy(lines)))
