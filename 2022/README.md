# Advent of Code 2022

My Advent of Code solutions for 2022.

- [X] [Day 1](./day-01/solution.py) - **Calorie Counting** has just some list manipulations
- [X] [Day 2](./day-02/solution.py) - **Rock Paper Scissors** has a fun use of overriding > and == operators
- [X] [Day 3](./day-03/solution.py) - **Rucksack Reorganization** uses Python's `set` to find intersections of character sets 
- [X] [Day 4](./day-04/solution.py) - **Camp Cleanup** is really just some Boolean expressions
- [X] [Day 5](./day-05/solution.py) - **Supply Stacks** is really just stack flooping
- [X] [Day 6](./day-06/solution.py) - **Tuning Trouble** used Python's `set` to find substrings of unique characters
- [X] [Day 7](./day-07/solution.py) - **No Space Left On Device** used a dictionary as a nice way to describe a file system
- [X] [Day 8](./day-08/solution.py) - **Treetop Tree House**
- [X] [Day 9](./day-09/solution.py) - **Rope Bridge**
- [ ] Day 10
- [ ] Day 11
- [ ] Day 12
- [ ] Day 13
- [ ] Day 14
- [ ] Day 15
- [ ] Day 16
- [ ] Day 17
- [ ] Day 18
- [ ] Day 19
- [ ] Day 20
- [ ] Day 21
- [ ] Day 22
- [ ] Day 23
- [ ] Day 24
- [ ] Day 25
