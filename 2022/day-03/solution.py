def has_priority(common):
    lower = "A"
    offset = 27
    if "a" <= common <= "z":
        lower = "a"
        offset = 1
    return ord(common) - ord(lower) + offset


def solution1(rucks: list[str]) -> int:
    priority = 0
    for ruck in rucks:
        common = (
            set(ruck[: len(ruck) // 2]) & set(ruck[len(ruck) // 2 :])
        ).pop()
        priority += has_priority(common)
    return priority


def solution2(rucks: list[str]) -> int:
    groups = [rucks[i : i + 3] for i in range(0, len(rucks), 3)]
    priority = 0
    for a, b, c in groups:
        common = (set(a) & set(b) & set(c)).pop()
        priority += has_priority(common)
    return priority


if __name__ == "__main__":
    with open("input.txt") as f:
        rucks = [line.strip() for line in f.readlines()]
        priority = solution1(rucks)
        print(priority)
        group_priority = solution2(rucks)
        print(group_priority)
