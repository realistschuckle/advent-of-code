class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def height(self):
        return abs(self.y)

    @property
    def width(self):
        return abs(self.x)

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __iter__(self):
        yield self.x
        yield self.y

    def __str__(self):
        return f"<{self.x}, {self.y}>"


def solution1(dirs: list[tuple[str, int]]) -> int:
    deltas = {
        "R": Vector(1, 0),
        "L": Vector(-1, 0),
        "U": Vector(0, 1),
        "D": Vector(0, -1),
    }
    head = Vector(0, 0)
    tail = Vector(0, 0)
    positions = set([tuple(tail)])
    for direction, amount in dirs:
        delta = deltas[direction]
        for _ in range(amount):
            head += delta
            distance = head - tail
            if distance.width == 0 and distance.height == 2:
                tail += Vector(0, 1 if distance.y > 0 else -1)
            elif distance.height == 0 and distance.width == 2:
                tail += Vector(1 if distance.x > 0 else -1, 0)
            elif distance.height == 2 or distance.width == 2:
                tail += Vector(
                    1 if distance.x > 0 else -1,
                    1 if distance.y > 0 else -1,
                )
            positions.add(tuple(tail))
    return len(positions)


class Knot:
    def __init__(self, x, y):
        self.next = None
        self.x = x
        self.y = y

    def __iter__(self):
        yield self.x
        yield self.y

    def move(self, dx, dy):
        self.x += dx
        self.y += dy
        if self.next:
            self.next.drag(self.x, self.y)

    def drag(self, x, y):
        dx = x - self.x
        dy = y - self.y
        if abs(dx) == 2 and dy == 0:
            self.x += 1 if dx > 0 else -1
        elif abs(dy) == 2 and dx == 0:
            self.y += 1 if dy > 0 else -1
        elif abs(dx) == 2 or abs(dy) == 2:
            self.x += 1 if dx > 0 else -1
            self.y += 1 if dy > 0 else -1
        if self.next:
            self.next.drag(self.x, self.y)

    def __repr__(self):
        return f"<{self.x}, {self.y}>"


def print_board(positions, top, right, bottom, left):
    for i in range(bottom, top + 1):
        for j in range(left, right + 1):
            if (j, i) in positions:
                print("#", end="")
            else:
                print(".", end="")
        print("")


def solution2(dirs: list[tuple[str, int]]) -> int:
    deltas = {
        "R": (1, 0),
        "L": (-1, 0),
        "U": (0, 1),
        "D": (0, -1),
    }
    knot = head = Knot(0, 0)
    for _ in range(8):
        knot.next = Knot(0, 0)
        knot = knot.next
    knot.next = tail = Knot(0, 0)
    positions = set([tuple(tail)])
    for direction, amount in dirs:
        delta = deltas[direction]
        for _ in range(amount):
            head.move(*delta)
            positions.add(tuple(tail))
    return len(positions)


if __name__ == "__main__":
    with open("input.txt") as f:
        lines = [l.strip() for l in f.readlines()]
        dirs = [(a, int(b)) for a, b in [l.split(" ") for l in lines]]
        print(solution1(dirs))
        print(solution2(dirs))
