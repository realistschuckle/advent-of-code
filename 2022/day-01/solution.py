def solution1(calories: list[int | None]) -> int:
    most_calories = 0
    current_calories = 0
    for num in calories:
        if num is not None:
            current_calories += num
        else:
            if current_calories > most_calories:
                most_calories = current_calories
            current_calories = 0
    return most_calories


def solution2(calories: list[int | None]) -> int:
    calories_by_elf = []
    current_elf = []
    for num in calories:
        if num is not None:
            current_elf.append(num)
        else:
            calories_by_elf.append(current_elf)
            current_elf = []
    return sum(
        list(sorted([sum(x) for x in calories_by_elf], reverse=True))[0:3]
    )


if __name__ == "__main__":
    with open("input.txt") as f:
        calories = [int(num) if num.strip() else None for num in f.readlines()]
        most_calories = solution1(calories)
        print(most_calories)
        top_3_calories = solution2(calories)
        print(top_3_calories)
