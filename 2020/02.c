#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PASSWORD_LENGTH 100

struct entry {
  int  min;
  int  max;
  char letter;
  char password[PASSWORD_LENGTH];
};

bool validateWrong(struct entry *e) {
  int count = 0;
  for (int i = 0, c = e->password[i]; c != '\0' && i < PASSWORD_LENGTH; i += 1, c = e->password[i]) {
    if (e->letter == c) {
      count += 1;
    }
  }
  return e->min <= count && count <= e->max;
}

void validatePasswordWithWrongAlgorithm(struct entry* entries, int length) {
  int totalValid = 0;
  for (int i = 0; i < length; i += 1) {
    struct entry* e = &entries[i];
    bool isValid = validateWrong(e);
    totalValid += (isValid ? 1 : 0);
  }
  printf("%d\n", totalValid);
}

bool validateRight(struct entry *e) {
  bool minGood = e->password[e->min - 1] == e->letter;
  bool maxGood = e->password[e->max - 1] == e->letter;
  return (minGood || maxGood) && (minGood != maxGood);
}

void validatePasswordsWithRightAlgorithm(struct entry* entries, int length) {
  int totalValid = 0;
  for (int i = 0; i < length; i += 1) {
    struct entry* e = &entries[i];
    bool isValid = validateRight(e);
   totalValid += (isValid ? 1 : 0);
  }
  printf("%d\n", totalValid);
}

int main() {
  const char* FILE_NAME = "02-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s", FILE_NAME);
    exit(1);
  }

  int lineCount = 0;
  char c;
  while (EOF != (c = fgetc(input))) {
    lineCount += (c == '\n' ? 1 : 0);
  }

  fseek(input, 0, SEEK_SET);
  struct entry* entries = calloc(lineCount, sizeof(struct entry));
  for (int i = 0; i < lineCount; i += 1) {
    struct entry* e = &entries[i];
    int min;
    int max;
    char letter;
    memset(&(e->password), 0, PASSWORD_LENGTH);
    fscanf(input, "%d-%d %c: %[^\n]\n", &(e->min), &(e->max), &(e->letter), &(e->password));
  }

  validatePasswordWithWrongAlgorithm(entries, lineCount);
  validatePasswordsWithRightAlgorithm(entries, lineCount);

  fclose(input);
  free(entries);
  return 0;
}
