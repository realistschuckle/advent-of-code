#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_LEN 30

int readPositiveNumber(FILE* input, char delim) {
  int columnIndex = 0;
  char line[MAX_NUM_LEN] = { 0 };

  char c;
  for (c = fgetc(input); c != EOF && c != delim; c = fgetc(input)) {
    line[columnIndex] = c;
    columnIndex += 1;
  }

  if (c == EOF && strlen(line) == 0) {
    return -1;
  }

  return atoi(line);
}

