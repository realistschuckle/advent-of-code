#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void quicksort(int* a, int lo, int hi);
extern int readPositiveNumber(FILE* input, char delim);

#define MAX_ADAPTERS 128

int diffPrev(int* a, int index) {
  return a[index] - a[index - 1];
}

long calcConfigs(int* a, int left, int right) {
  int width = right - left;
  if (a[left + 1] - a[left] == 3 && width == 1) {
    return 1;
  } else if (a[left + 1] - a[left] == 3) {
    return calcConfigs(a, left + 1, right);
  }
  if (width == 2) {
    return 1;
  } else if (width == 3 && a[right - 1] - a[left] <= 3) {
    return 2;
  } else if (width == 3) {
    return 1;
  }
  
  int first = calcConfigs(a, left + 1, right);

  int diff = a[left + 1] - a[left];
  a[left + 1] -= diff;
  int second = calcConfigs(a, left + 1, right);
  a[left + 1] += diff;
  return first + second;
}

int main() {
  const char* FILE_NAME = "10-input.txt";

  FILE* input;
  if ((input = fopen(FILE_NAME, "r")) == NULL) {
    printf("Error opening %s\n", FILE_NAME);
    return 1;
  }

  int adapterJolts[MAX_ADAPTERS];
  memset(adapterJolts, -1, MAX_ADAPTERS * sizeof(int));
  adapterJolts[0] = 0;
  int i;
  int j;
  for (i = 1, j = readPositiveNumber(input, '\n'); i < MAX_ADAPTERS && j > 0; i += 1, j = readPositiveNumber(input, '\n')) {
    adapterJolts[i] = j;
  }

  fclose(input);

  quicksort(adapterJolts, 0, i - 1);
  adapterJolts[i] = adapterJolts[i - 1] + 3;

  int sumOneDiffs = 0;
  int sumThreeDiffs = 0;
  for (int i = 1; adapterJolts[i] >= 0; i += 1) {
    int diff = adapterJolts[i] - adapterJolts[i - 1];
    if (diff == 1) {
      sumOneDiffs += 1;
    } else if (diff == 3) {
      sumThreeDiffs += 1;
    } else {
      printf("ERROR: Found a diff of %d\n", diff);
      printf("Indexes %d and %d\n", i - 1, i);
      printf("With values %d and %d\n", adapterJolts[i - 1], adapterJolts[i]);
    }
  }
  printf("%d\n", sumOneDiffs * sumThreeDiffs);

  long product = 1;
  int string = 0;
  for (int left = 0, right = 1; adapterJolts[right] >= 0; right += 1) {
    if (diffPrev(adapterJolts, right) == 3) {
      product *= calcConfigs(adapterJolts, left, right);
      left = right;
      right = right + 1;
    } else if (diffPrev(adapterJolts, right) == 2 && diffPrev(adapterJolts, right + 1) == 2) {
      product *= calcConfigs(adapterJolts, left, right);
      left = right + 1;
      right = right + 2;
    }
  }
  printf("%ld\n", product);
  
return 0;
}

