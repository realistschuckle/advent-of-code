import itertools
import logging


invalid_chars = [c - ord("a") for c in [ord("i"), ord("o"), ord("l")]]


def has_straight(chars):
    return sum([1 for a, b in itertools.pairwise(chars) if a - b == 1]) >= 3


def has_valid_chars(chars):
    invalid = set(invalid_chars)
    return len(set(chars) & invalid) == 0


def has_repeat(chars):
    i = 0
    count = 0
    while i < len(chars) - 1:
        if chars[i] == chars[i + 1]:
            count += 1
            i += 1
        i += 1
    return count >= 2


def passes_first_requirements(chars):
    return has_straight(chars) and has_valid_chars(chars) and has_repeat(chars)


def decode(chars: list[int]) -> str:
    return "".join(reversed([chr(c + ord("a")) for c in chars]))


def solution1(start: str) -> int:
    chars = list(reversed([ord(c) - ord("a") for c in list(start)]))
    while not passes_first_requirements(chars):
        logging.info(
            "%s %s %s %s",
            has_straight(chars),
            has_valid_chars(chars),
            has_repeat(chars),
            decode(chars),
        )
        chars[0] += 1
        for i, c in enumerate(chars):
            if c == 26:
                chars[i] = 0
                chars[i + 1] += 1
            elif c in invalid_chars:
                chars[i] += 1
                for j in range(i):
                    chars[j] = 0
    return decode(chars)
        


if __name__ == "__main__":
    import sys
    if "DEBUG" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)

    start = "hxbxwxba"
    # start = "ghijklmn"
    # start = "abcdefgh"

    print(solution1(start))
