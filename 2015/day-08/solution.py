def solution1(lines: list[str]) -> int:
    code = 0
    memory = 0
    for line in lines:
        code += len(line)
        memory += len(eval(line))
    return code - memory


def solution2(lines: list[str]) -> int:
    code = 0
    encoded = 0
    for line in lines:
        replaced = '"' + line.replace("\\", "\\\\").replace("\"", "\\\"") + '"'
        code += len(line)
        encoded += len(replaced)
    return encoded - code


if __name__ == "__main__":
    with open("input.txt") as f:
        lines = [l.strip() for l in f.readlines()]
        print(solution1(lines))
        print(solution2(lines))
