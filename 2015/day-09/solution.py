import itertools


def build_map(lines: list[list[str]]) -> tuple[set, dict[str, dict[str, int]]]:
    cities = set()
    distances = {}
    
    for line in lines:
        cities.add(line[0])
        cities.add(line[2])
        if line[0] not in distances:
            distances[line[0]] = {}
        distances[line[0]][line[2]] = int(line[4])
        if line[2] not in distances:
            distances[line[2]] = {}
        distances[line[2]][line[0]] = int(line[4])

    return cities, distances


def solution1(lines: list[list[str]]) -> int:
    cities, distances = build_map(lines)
    min_distance = sum([d for c in distances.values() for d in c.values()])

    for path in itertools.permutations(sorted(cities)):
        path_total = 0
        for leg in itertools.pairwise(path):
            path_total += distances[leg[0]][leg[1]]
        if path_total < min_distance:
            min_distance = path_total

    return min_distance


def solution2(lines: list[list[str]]) -> int:
    cities, distances = build_map(lines)
    max_distance = 0

    for path in itertools.permutations(sorted(cities)):
        path_total = 0
        for leg in itertools.pairwise(path):
            path_total += distances[leg[0]][leg[1]]
        if path_total > max_distance:
            max_distance = path_total

    return max_distance

    return 0


if __name__ == "__main__":
    with open("input.txt") as f:
        lines = [l.strip().split() for l in f.readlines()]
        print(solution1(lines))
        print(solution2(lines))
