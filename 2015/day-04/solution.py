import hashlib


def solution1(key):
    secret = 0
    value = bytes(key + str(secret), "utf-8")
    while not hashlib.md5(value).hexdigest().startswith("00000"):
        secret += 1
        value = bytes(key + str(secret), "utf-8")
    return secret


def solution2(key):
    secret = 0
    value = bytes(key + str(secret), "utf-8")
    while not hashlib.md5(value).hexdigest().startswith("000000"):
        secret += 1
        value = bytes(key + str(secret), "utf-8")
    return secret


if __name__ == "__main__":
    with open("input.txt") as f:
        key = f.read().strip()
        print(solution1(key))
        print(solution2(key))
