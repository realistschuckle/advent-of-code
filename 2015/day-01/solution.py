def solution1(instructions: str) -> int:
    floor = 0
    for c in instructions:
        if c == "(":
            floor += 1
        elif c == ")":
            floor -= 1
    return floor


def solution2(instructions: str) -> int:
    floor = 0
    for i, c in enumerate(instructions):
        if c == "(":
            floor += 1
        elif c == ")":
            floor -= 1
        if floor < 0:
            return i + 1
    raise RuntimeError()


if __name__ == "__main__":
    with open("input.txt") as f:
        instructions = f.read()
        print(solution1(instructions))
        print(solution2(instructions))
