from itertools import pairwise


def solution1(lines):
    total = 0
    for line in lines:
        if line.find("ab") > -1 or line.find("cd") > -1 or line.find("pq") > -1 or line.find("xy") > -1:
            continue
        if line.count("a") + line.count("e") + line.count("i") + line.count("o") + line.count("u") < 3:
            continue
        if len([x for x in pairwise(line) if x[0] == x[1]]) == 0:
            continue
        total += 1
    return total


def pair_repeats(line: str) -> bool:
    for i in range(len(line) - 1):
        if line.count(line[i:i+2]) > 1:
            return True
    return False


def letter_repeats(line: str) -> bool:
    for i, c in enumerate(line[:-2]):
        if line[i + 2] == c:
            return True
    return False


def solution2(lines):
    total = 0
    for line in lines:
        if pair_repeats(line) and letter_repeats(line):
            total += 1
    return total


if __name__ == "__main__":
    with open("input.txt") as f:
        lines = [l.strip() for l in f.readlines()]

        print(solution1(lines))
        print(solution2(lines))
