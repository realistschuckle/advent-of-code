import operator
import re


def repl(g: re.Match):
    return str(abs(operator.sub(*g.span()))) + g.group(0)[0]


def look_and_say(s: str):
    pattern = re.compile(r"(.)\1*")
    return pattern.sub(repl, s)


def solution1(start: str) -> int:
    value = start
    for _ in range(40):
        value = look_and_say(value)
    return len(value), value


def solution2(start: str) -> int:
    value = start
    for _ in range(10):
        value = look_and_say(value)
    return len(value)



if __name__ == "__main__":
    start = "1113122113"

    length, value = solution1(start)
    print(length)
    print(solution2(value))
