from itertools import chain
from typing import Callable


LightSwitch = Callable[[list[list[bool]], int, int], bool]
Coordinate = tuple[int, int]


def solution1(lines: list[tuple[LightSwitch, Coordinate, Coordinate]]) -> int:
    lights = []
    for i in range(1000):
        lights.append([False]*1000)
    for line in lines:
        for row in range(line[1][0], line[2][0] + 1):
            for col in range(line[1][1], line[2][1] + 1):
                lights[row][col] = line[0](lights, row, col)
    return sum([int(v) for v in chain(*lights)])


def solution2(lines: list[tuple[LightSwitch, Coordinate, Coordinate]]) -> int:
    lights = []
    for i in range(1000):
        lights.append([0]*1000)
    for line in lines:
        for row in range(line[1][0], line[2][0] + 1):
            for col in range(line[1][1], line[2][1] + 1):
                lights[row][col] = max(lights[row][col] + line[0](), 0)
    return sum(chain(*lights))


def as_coord(spec: str) -> Coordinate:
    return tuple([int(x) for x in spec.split(",")])


if __name__ == "__main__":
    fns = {
        1: {
            "on": lambda d, x, y: True,
            "off": lambda d, x, y: False,
            "toggle": lambda d, x, y: not d[x][y],
        },
        2: {
            "on": lambda: 1,
            "off": lambda: -1,
            "toggle": lambda: 2,
        },
    }
    with open("input.txt") as f:
        lines = [l.strip().split() for l in f.readlines()]
        lines1 = [
            (
                fns[1]["toggle"],
                as_coord(l[1]),
                as_coord(l[3]),
            )
            if l[0] == "toggle"
            else (
                fns[1][l[1]],
                as_coord(l[2]),
                as_coord(l[4]),
            )
            for l in lines
        ]
        lines2 = [
            (
                fns[2]["toggle"],
                as_coord(l[1]),
                as_coord(l[3]),
            )
            if l[0] == "toggle"
            else (
                fns[2][l[1]],
                as_coord(l[2]),
                as_coord(l[4]),
            )
            for l in lines
        ]
        print(solution1(lines1))
        print(solution2(lines2))
