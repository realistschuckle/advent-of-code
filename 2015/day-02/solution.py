class Box:
    def __init__(self, *measures):
        measures = list(sorted([int(m) for m in measures]))
        self.h = measures[0]
        self.w = measures[1]
        self.d = measures[2]

    @property
    def area(self):
        h = self.h
        w = self.w
        d = self.d
        return 2 * (h * w + w * d + h * d)

    @property
    def extra(self):
        return self.h * self.w

    @property
    def shortest_perimeter(self):
        return 2 * (self.h + self.w)

    @property
    def volume(self):
        return self.h * self.w * self.d

    @property
    def total(self):
        return self.area + self.extra

    def __repr__(self):
        return f"<Box ({self.area}) {self.h} {self.w} {self.d}>"


def solution1(boxes: list[Box]) -> int:
    return sum([box.total for box in boxes])


def solution2(boxes: list[Box]) -> int:
    return sum([
        box.shortest_perimeter + box.volume
        for box in boxes
    ])


if __name__ == "__main__":
    with open("input.txt") as f:
        boxes = [
            Box(*line.strip().split("x"))
            for line in f.readlines()
        ]
        print(solution1(boxes))
        print(solution2(boxes))
