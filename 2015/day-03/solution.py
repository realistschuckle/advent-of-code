def solution1(directions: str) -> int:
    pos = (0, 0)
    houses = {(0, 0): 1}
    for direction in directions:
        if direction == "^":
            pos = pos[0], pos[1] - 1
        elif direction == "v":
            pos = pos[0], pos[1] + 1
        elif direction == ">":
            pos = pos[0] + 1, pos[1]
        elif direction == "<":
            pos = pos[0] - 1, pos[1]
        if pos not in houses:
            houses[pos] = 0
        houses[pos] += 1
    return len(houses)


def solution2(directions: str):
    poses = [(0, 0), (0, 0)]
    houses = {(0, 0): 2}
    for i, direction in enumerate(directions):
        pos = poses[i % 2]
        if direction == "^":
            pos = pos[0], pos[1] - 1
        elif direction == "v":
            pos = pos[0], pos[1] + 1
        elif direction == ">":
            pos = pos[0] + 1, pos[1]
        elif direction == "<":
            pos = pos[0] - 1, pos[1]
        if pos not in houses:
            houses[pos] = 0
        houses[pos] += 1
        poses[i % 2] = pos
    return len(houses)


if __name__ == "__main__":
    with open("input.txt") as f:
        directions = f.read()
        print(solution1(directions))
        print(solution2(directions))
