import logging
import operator
import re
from typing import Callable


def complement(n):
    return ((1 << 16) - 1) ^ n;


def input(value: str):
    cached = None
    def activate(d: dict, s: str):
        nonlocal cached
        if cached is None:
            activated = True
            logging.info(f"{s} <- {value}")
            cached = d.get(value, lambda x, y: int(value))(d, value)
        return cached

    return activate


def binary(left: str, right: str, op: Callable[[int, int], int]):
    cached = None
    def activate(d: dict, s: str):
        nonlocal cached
        if cached is None:
            logging.info(f"{s} <- {left} {op.__name__} {right}")
            l = d.get(left, lambda x, y: int(left))(d, left)
            r = d.get(right, lambda x, y: int(right))(d, right)
            cached = op(l, r) & 65535
        return cached

    return activate


def nope(operand: str):
    cached = None
    def activate(d: dict, s: str):
        nonlocal cached
        if cached is None:
            logging.info(f"{s} <- NOT {operand}")
            v = d[operand](d, operand)
            cached = ((1 << 16) - 1) ^ v
        return cached

    return activate


def make_circuits(lines: list[str]):
    d = {}
    for line in lines:
        if len(line) == 3:
            d[line[2]] = input(line[0])
        elif len(line) == 4:
            d[line[3]] = nope(line[1])
        else:
            match line[1]:
                case "AND":
                    op = operator.and_
                case "OR":
                    op = operator.or_
                case "LSHIFT":
                    op = operator.lshift
                case "RSHIFT":
                    op = operator.rshift
                case _:
                    raise RuntimeError(line)
            d[line[4]] = binary(line[0], line[2], op)
    return d


def solution1(lines: list[str]) -> int:
    d = make_circuits(lines)
    return d["a"](d, "a")


def solution2(b: int, lines: list[str]) -> int:
    d = make_circuits(lines)
    d["b"] = input(b)
    return d["a"](d, "a")


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1 and "DEBUG" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
    with open("input.txt") as f:
        lines = [l.strip().split() for l in f.readlines()]
        value = solution1(lines)
        print(value)
        value = solution2(value, lines)
        print(value)
