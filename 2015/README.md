# Advent of Code 2015

My Advent of Code solutions for 2015.

- [X] [Day 1](./day-01/solution.py) **Not Quite Lisp** nothing exciting here
- [X] [Day 2](./day-02/solution.py) **I Was Told There Would Be No Math** which is just a great name
- [X] [Day 3](./day-03/solution.py) **Perfectly Spherical Houses in a Vacuum**
- [X] [Day 4](./day-04/solution.py) **The Ideal Stocking Stuffer**
- [X] [Day 5](./day-05/solution.py) **Doesn't He Have Intern-Elves For This?**
- [X] [Day 6](./day-06/solution.py) **Probably a Fire Hazard**
- [X] [Day 7](./day-07/solution.py) **Some Assembly Required** a functional AST with some dynamic programming
- [X] [Day 8](./day-08/solution.py) **Matchsticks** says, "eval is not evil..."
- [X] [Day 9](./day-09/solution.py) **All in a Single Night** with some brute-force n! stuff going on
- [X] [Day 10](./day-10/solution.py) **Elves Look, Elves Say** has an implementation of the [OEIS A006715](https://oeis.org/A006715) sequence
- [ ] [Day 11](./day-11/solution.py) **Corporate Policy**
- [ ] Day 12
- [X] [Day 13](./day-13/solution.py) **Knights of the Dinner Table** - permutations and pairwise processing (An interesting story? I was trying to solve Day 13, 2021, but somehow my browser showed me 2015, so here's my first entry in 2015? Okay, maybe not _that_ interesting.)
- [ ] Day 14
- [ ] Day 15
- [ ] Day 16
- [ ] Day 17
- [ ] Day 18
- [ ] Day 19
- [ ] Day 20
- [ ] Day 21
- [ ] Day 22
- [ ] Day 23
- [ ] Day 24
- [ ] Day 25
