# Advent of Code Mono Repo

I got tired of having separate repositories for each year of Advent of Code,
so, this is just one.

- [ ] [2023](./2023) - Haskell
- [ ] [2022](./2022) - Another year of Python
- [ ] [2021](./2021) - The year of Python
- [ ] [2020](./2020) - The year of C
- [ ] [2019](./2019) - The year of ECMAScript
- [ ] [2018](./2018) - The year of Go
- [ ] 2017
- [ ] 2016
- [ ] [2015](./2015) - Accidentally done in Python
